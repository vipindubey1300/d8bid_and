export default {
  //welcome
  welcome_sign_in: '登入',
  welcome_sign_up: '注册',

//login
  email_id: '电子邮件ID',
  password:'密码',
  forgot_password:"忘记密码",
  login_with:'或者登录',
  dont_have_account:"没有帐户",
  sign_up:'注册',
  login:'登录',

//forgot password
  enter_registered_email:'输入注册电邮',
  send_my_password:'发送我的密码',

//enter otp
  password_recovery: '找回密码',
  enter_otp:'输入OTP',
  resend_otp:'重新发送OTP',

//otpshcnagepassword
  please_change_your_password:'请更改您的密码',
  new_password:'新密码',
  confirm_password:'确认密码',
  change:'更改',

  //location /location Socailamember
  profile_details:'资料详情',
  country:'国家',
  state:'州',
  city:'市',
  gender:'性别',
  interested_gender:'有兴趣的性别',
  male:'男',
  female:'女',
  transgender:'变性',


  //createprofiel /both
  create_profile:'创建个人资料',
  add_images:'添加图片',
  model_display_name:'型号显示名称',
  member_display_name:'会员显示名称',
  age:'年龄',
  height_cms:'高度（以厘米为单位）',
  weight_kgs:'重量（以公斤为单位）',
  about_bio:'关于Bio（可选）',
  ethnicity:'种族',
  body_type:'体型',
  save:'保存',

  //choose member type both
  who_you_are:'你是谁',
  please_select_type:'请选择您的类型',
  enter_as_model:'输入为模型',
  enter_as_member:'输入为会员',

  //signup member and sign up model
  app_user_name:'应用用户名',
  email:'电子邮件',
  phone_no:'电话号码。',
  password:'密码',
  confirm_password:'确认密码',
  accept_terms_condition:'接受条款和条件',
  or_signup_with:'或者用SignUp',
  already_have_account :'已经有一个帐户',
  log_in:'登录',

  //instagram email
  enter_your_email:'输入您的电子邮件ID',
  submit:'提交',

  //homepasge model/member
  na:'N/A',
  height:'高度',
  weight:'重量',
  cms:'cms',
  kgs:'公斤',
  about:'关于',
  available_now:'现在有空',
  available_later:'稍后可用',

  //profile_details
  model_profile:'型号简介',
  member_profile:'会员档案',
  set_bid_amount:'设置出价金额',
  bid_now:'现在出价',
  choose_bid_amount_dollars:'选择出价金额（US $）',
  select:'选择',
  your_previous_bid_amount:'您之前的出价金额',
  raised_bid_amount:'提高的投标金额',


  //reset password
  reset_password:'重设密码',
  please_update_your_password:'请更新您的密码',
  old_password:'旧密码',
  new_password:'新密码',
  confirm_password:'确认密码',
  reset:'重启',

  //profile model
  profile:'轮廓',
  edit:'编辑',
  location:'地点',

  //payment
  payment_mode:'付款方式',
  select_payment_mode:'选择付款方式',
  credit_debit:'信用卡/借记卡',
  paypal:'贝宝',
  credit_card:'信用卡',

  //model rating
  rate:'率',

  //model profile chat.
  redeem_one_coin_chat:'兑换1枚硬币进行聊天',
  redeem_one_coin_chat_to_lock:'兑换1枚硬币以锁定已接受的出价并接收模型私人联系方式',
  redeem_now:'立即兑换',
  no_coins_left:'没有硬币了',
  buy_coins_to_connect:'请购买硬币连接',
  buy_coins:'买硬币',


  //filters
  choose_desired_location_for_models:'选择所需的城市以获取可用模型的列表',
  cities:'城市',
  filters:'过滤器',
  change:'更改',
  search_by_stars:'Search By Stars',
  close:'关',


  //edit profile both
  edit_profile:'编辑个人资料',
  name:'名称',
  save:'保存',

  //contact us
  contact_us:'联系我们',
  message_title:'消息标题',
  description:'描述',
  model_member_complain:'型号/会员投诉',
  username:'用户名',
  model_member_noshow:'型号/会员没有展示',
  
  model_accepted_bid:'模型接受的出价不符合',
  member_send_bid:'成员发送投标未满足',
  bad_behaviour:'不良行为或暴力',
  model_paid_coin:'模型付费硬币',
  model_didnot_meet:'模特没见面',
  please_detail_complain:'请详细说明您的投诉',
  objectional_content:'客观内容，虐待或暴力',


  //buy coins and stars
  buy_coins:'买硬币',
  buy_stars:'买星星',
  buy_coins_to_unlock_conversation:'购买硬币解锁对话并开始第一次约会！',
  choose_number_of_coins:'选择硬币数量。',
  buy_now:'立即购买',
  total_price:'总价',
  buy_star_rating_visible_partner:'购买星级评级，让合作伙伴更加明显！',
  choose_number_of_stars:'选择星数。',


//notifcaiton
  enter_raise_amount:'输入提高金额',
  enter_amount_dollars:'输入金额美元',
  raise:'提高',
  raised:'提高',
  accepted:'公认',
  rejected:'被拒绝',
  bid_price:'竞标价格',
  pending:'有待',

  //cchat lit
  chat_list : "聊天列表",

  //choose language
  choose_language:'选择语言',
  language:'语言',

  //drawer both
  home:'家',
  chats:'聊天',
  coins:'硬币',
  stars:'明星',
  notifications:'通知',
  update_profile:'更新配置文件',
  change_password:'更改密码',
  legal:'法律',
  privacy_policy:'隐私政策',
  terms_and_condition:'条款与协议',
  logout:'登出',
  bids:'投标',

  //header
  lists : '清单',
  activity:'活动',

  //drawer
  coins:'硬币',
  stars:'明星',

  //location modal overlay
  choose:'选择',
  distance:'距离',
  choose_distance:'选择距离（km）',

  //block model
  block :'块',
  unblock:'解除封锁',
  choose_block_reason:'输入要阻止的原因（可选）',
  block_this_model:'阻止此模型',

  //
  enter_otp_email:'请使用我们发送给您的OTP验证您的电子邮件。如果不在收件箱中，请检查垃圾邮件',


  //video
  add_video:'新增影片',
  see_video:'观看视频',
  upload:'上载',
  no_video_found:'找不到视频',

    //location
    send:'Send',
    your_current_location:'Your Current location :',
    nearby_favourite_places:'Nearby Favourite Places :',
    send_location:'Send Location',


};
