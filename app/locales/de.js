export default {
  //welcome
  welcome_sign_in: 'Einloggen',
  welcome_sign_up: 'Anmelden',

//login
  email_id: 'E-Mail-ID',
  password:'Passwort',
  forgot_password:"Passwort vergessen",
  login_with:'oder einloggen mit',
  dont_have_account:"Ich habe keinen Account",
  sign_up:'Anmelden',
  login:'Anmeldung',

//forgot password
  enter_registered_email:'Geben Sie eine registrierte E-Mail-Adresse ein',
  send_my_password:'Sende mein Passwort',

//enter otp
  password_recovery: 'Passwort-Wiederherstellung',
  enter_otp:'OTP eingeben',
  resend_otp:'OTP eingeben',

//otpshcnagepassword
  please_change_your_password:'Bitte ändern Sie Ihr Passwort',
  new_password:'Neues Kennwort',
  confirm_password:'Passwort bestätigen',
  change:'Veränderung',

  //location /location Socailamember
  profile_details:'Profildetails',
  country:'Land',
  state:'Zustand',
  city:'Stadt',
  gender:'Geschlecht',
  interested_gender:'Interessiertes Geschlecht',
  male:'Männlich',
  female:'Weiblich',
  transgender:'Transgender',


  //createprofiel /both
  create_profile:'Profil erstellen',
  add_images:'Füge Bilder hinzu',
  model_display_name:'Anzeigename des Modells',
  member_display_name:'Anzeigename des Mitglieds',
  age:'Alter',
  height_cms:'Höhe in cm)',
  weight_kgs:'Gewicht (in kg)',
  about_bio:'Über Bio (optional)',
  ethnicity:'Ethnische Zugehörigkeit',
  body_type:'Körpertyp',
  save:'sparen',

  //choose member type both
  who_you_are:'Wer du bist',
  please_select_type:'Bitte wählen Sie Ihren Typ',
  enter_as_model:'Als Model eintragen',
  enter_as_member:'Als Mitglied eintragen',

  //signup member and sign up model
  app_user_name:'App-Benutzername',
  email:'Email',
  phone_no:'Telefon-Nr.',
  password:'Passwort',
  confirm_password:'Passwort bestätigen',
  accept_terms_condition:'AGB akzeptieren',
  or_signup_with:'oder Anmelden mit',
  already_have_account :'Hast du schon ein Konto',
  log_in:'Einloggen',

  //instagram email
  enter_your_email:'Geben Sie Ihre E-Mail-ID ein',
  submit:'einreichen',

  //homepasge model/member
  na:'N/A',
  height:'Höhe',
  weight:'Gewicht',
  cms:'cms',
  kgs:'kgs',
  about:'Über',
  available_now:'Jetzt verfügbar',
  available_later:'Später verfügbar',

  //profile_details
  model_profile:'Modellprofil',
  member_profile:'Mitgliederprofil',
  set_bid_amount:'Gebotsbetrag festlegen',
  bid_now:'Bieten Sie jetzt',
  choose_bid_amount_dollars:'Gebotsbetrag auswählen (US $)',
  select:'Wählen',
  your_previous_bid_amount:'Ihr bisheriger Gebotsbetrag',
  raised_bid_amount:'Erhöhter Gebotsbetrag',


  //reset password
  reset_password:'Passwort zurücksetzen',
  please_update_your_password:'Bitte aktualisieren Sie Ihr Passwort',
  old_password:'Altes Passwort',
  new_password:'Neues Kennwort',
  confirm_password:'Passwort bestätigen',
  reset:'Zurücksetzen',

  //profile model
  profile:'Profil',
  edit:'Bearbeiten',
  location:'Ort',

  //payment
  payment_mode:'Zahlungsart',
  select_payment_mode:'Wählen Sie den Zahlungsmodus',
  credit_debit:'Kredit- / Debitkarte',
  paypal:'paypal',
  credit_card:'Kreditkarte',

  //model rating
  rate:'Bewertung',

  //model profile chat.
  redeem_one_coin_chat:'Löse 1 Münze für den Chat ein',
  redeem_one_coin_chat_to_lock:'Lösen Sie 1 Münze ein, um ein akzeptiertes Gebot zu erhalten und private Kontaktdaten zu erhalten',
  redeem_now:'Jetzt einlösen',
  no_coins_left:'Keine Münzen übrig',
  buy_coins_to_connect:'Bitte kaufen Sie Münzen, um eine Verbindung herzustellen',
  buy_coins:'Kaufe Münzen',


  //filters
  choose_desired_location_for_models:'Wählen Sie die gewünschten Städte aus, um eine Liste der verfügbaren Modelle zu erhalten',
  cities:'Städte',
  filters:'Filter',
  change:'Veränderung',
  search_by_stars:'Suche nach Sternen',
  close:'Schließen',


  //edit profile both
  edit_profile:'Profil bearbeiten',
  name:'Name',
  save:'sparen',

  //contact us
  contact_us:'Kontaktiere uns',
  message_title:'Nachrichtentitel',
  description:'Beschreibung',
  model_member_complain:'Model / Member Beschweren',
  username:'Nutzername',
  model_member_noshow:'Model / Mitglied No Show',
  model_accepted_bid:"Modell angenommenes Gebot nicht erfüllt",
  member_send_bid:'Gesendetes Gebot des Mitglieds nicht erfüllt',
  bad_behaviour:'Schlechtes Benehmen oder Gewalt',
  model_paid_coin:'Modell bezahlte Münze',
  model_didnot_meet:'Modell nicht erfüllt',
  please_detail_complain:'Bitte beschreiben Sie Ihre Beschwerde',
  objectional_content:'Widersprüchlicher Inhalt, Missbrauch oder Gewalt',


  //buy coins and stars
  buy_coins:'Kaufe Münzen',
  buy_stars:'Sterne kaufen',
  buy_coins_to_unlock_conversation:'Kaufen Sie Münzen, um Gespräche freizuschalten und erste Termine zu vereinbaren!',
  choose_number_of_coins:'Wählen Sie die Anzahl der Münzen.',
  buy_now:'Kaufe jetzt',
  total_price:'Gesamtpreis',
  buy_star_rating_visible_partner:'Kaufen Sie Sterne für mehr Sichtbarkeit für Ihre Partner!',
  choose_number_of_stars:'Wählen Sie die Anzahl der Sterne.',


//notifcaiton
  enter_raise_amount:'Betrag erhöhen eingeben',
  enter_amount_dollars:'Geben Sie den Betrag in Dollar ein',
  raise:'Erziehen',
  raised:'Angehoben',
  accepted:'Akzeptiert',
  rejected:'Abgelehnt',
  bid_price:'Angebotspreis',
  pending:'steht aus',

  //cchat lit
  chat_list : "Chat-Listen",

  //choose language
  choose_language:'Sprache wählen',
  language:'Sprache',

  //drawer both
  home:'Zuhause',
  chats:'Chats',
  coins:'Münzen',
  stars:'Sterne',
  notifications:'Benachrichtigungen',
  update_profile:'Profil aktualisieren',
  change_password:'Ändere das Passwort',
  legal:'Legal',
  privacy_policy:'Datenschutz-Bestimmungen',
  terms_and_condition:'Allgemeine Geschäftsbedingungen',
  logout:'Ausloggen',
  bids:'Gebote',

  //header
  lists : 'Listen',
  activity:'Aktivität',

  //drawer
  coins:'Münzen',
  stars:'Sterne',

  //location modal overlay
  choose:'Wählen',
  distance:'Entfernung',
  choose_distance:'Wähle Entfernung (km)',

  //block model
  block :'Block',
  unblock:'Entsperren',
  choose_block_reason:'Grund für die Sperrung eingeben (optional)',
  block_this_model:'Dieses Modell blockieren',

  //
  enter_otp_email:'Bitte bestätigen Sie Ihre E-Mail mit OTP, das wir Ihnen gesendet haben. Überprüfen Sie Junk-Mails, wenn sie nicht im Posteingang sind',

    //video
    add_video:'Video hinzufügen',
    see_video:'Siehe Video',
    upload:'Hochladen',
    no_video_found:'Kein Video gefunden',

      //location
  send:'Send',
  your_current_location:'Your Current location :',
  nearby_favourite_places:'Nearby Favourite Places :',
  send_location:'Send Location',


};
