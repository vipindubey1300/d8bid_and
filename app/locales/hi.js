export default {
  //welcome
  welcome_sign_in: 'दाखिल करना',
  welcome_sign_up: 'साइन अप करें',

//login
  email_id: 'ईमेल आईडी',
  password:'पारण शब्द',
  forgot_password:"पासवर्ड भूल गए",
  login_with:'या साथ लॉगिन करें',
  dont_have_account:"खाता नहीं है",
  sign_up:'साइन अप करें',
  login:'लॉग इन करें',

//forgot password
  enter_registered_email:'पंजीकृत ईमेल दर्ज करें',
  send_my_password:'मेरा पासवर्ड भेजें',

//enter otp
  password_recovery: 'पासवर्ड की दोबारा प्राप्ति',
  enter_otp:'OTP दर्ज करें',
  resend_otp:'OTP को फिर से भेजें',

//otpshcnagepassword
  please_change_your_password:'कृपया अपना पासवर्ड बदलें',
  new_password:'नया पासवर्ड',
  confirm_password:'पासवर्ड की पुष्टि कीजिये',
  change:'परिवर्तन',

  //location /location Socailamember
  profile_details:'प्रोफ़ाइल विवरण',
  country:'देश',
  state:'राज्य',
  city:'शहर',
  gender:'लिंग',
  interested_gender:'इच्छुक लिंग',
  male:'पुरुष',
  female:'महिला',
  transgender:'ट्रांसजेंडर',


  //createprofiel /both
  create_profile:'प्रोफ़ाइल बनाने',
  add_images:'छवियां जोड़ें',
  model_display_name:'मॉडल प्रदर्शन नाम',
  member_display_name:'सदस्य प्रदर्शन नाम',
  age:'आयु',
  height_cms:'लम्बाई (सेंटीमीटर मे)',
  weight_kgs:'वजन (किलोग्राम में)',
  about_bio:'जैव के बारे में (वैकल्पिक)',
  ethnicity:'जातीयता',
  body_type:'शरीर के प्रकार',
  save:'बचाना',

  //choose member type both
  who_you_are:'जो आप हैं',
  please_select_type:'कृपया अपने प्रकार का चयन करें',
  enter_as_model:'एक मॉडल के रूप में दर्ज करें',
  enter_as_member:'एक सदस्य के रूप में दर्ज करें',

  //signup member and sign up model
  app_user_name:'ऐप उपयोगकर्ता का नाम',
  email:'ईमेल',
  phone_no:'फोन नंबर',
  password:'पारण शब्द',
  confirm_password:'पासवर्ड की पुष्टि कीजिये',
  accept_terms_condition:'नियम और शर्त स्वीकार करें',
  or_signup_with:'या साइनअप के साथ',
  already_have_account :'पहले से ही एक खाता है',
  log_in:'लॉग इन करें',

  //instagram email
  enter_your_email:'अपनी ईमेल आईडी दर्ज करें',
  submit:'जमा करें',

  //homepasge model/member
  na:'एन / ए',
  height:'ऊंचाई',
  weight:'वजन',
  cms:'सेमी',
  kgs:'किलोग्राम',
  about:'के बारे में',
  available_now:'अब उपलब्ध है',
  available_later:'बाद में उपलब्ध',

  //profile_details
  model_profile:'मॉडल प्रोफ़ाइल',
  member_profile:'सदस्य प्रोफ़ाइल',
  set_bid_amount:'बोली राशि निर्धारित करें',
  bid_now:'अभी बोली लगाएँ',
  choose_bid_amount_dollars:'बोली राशि चुनें (US $)',
  select:'चुनते हैं',
  your_previous_bid_amount:'आपकी पिछली बोली राशि',
  raised_bid_amount:'बढ़ाकर बोली राशि',


  //reset password
  reset_password:'पासवर्ड रीसेट',
  please_update_your_password:'कृपया अपना पासवर्ड अपडेट करें',
  old_password:'पुराना पासवर्ड',
  new_password:'नया पासवर्ड',
  confirm_password:'पासवर्ड की पुष्टि कीजिये',
  reset:'रीसेट',

  //profile model
  profile:'प्रोफाइल',
  edit:'संपादित करें',
  location:'स्थान',

  //payment
  payment_mode:'भुगतान का प्रकार',
  select_payment_mode:'भुगतान मोड का चयन करें',
  credit_debit:'क्रेडिट / डेबिट कार्ड',
  paypal:'Paypal',
  credit_card:'क्रेडिट कार्ड',

  //model rating
  rate:'मूल्यांकन करें',

  //model profile chat.
  redeem_one_coin_chat:'चैट के लिए 1 सिक्का रिडीम करें',
  redeem_one_coin_chat_to_lock:'स्वीकार किए गए बोली में लॉक करने के लिए 1 मॉडल को रिडीम करें और मॉडल से निजी संपर्क विवरण प्राप्त करें',
  redeem_now:'अब एवज करें',
  no_coins_left:'कोई सिक्के वाम नहीं',
  buy_coins_to_connect:'कनेक्ट करने के लिए सिक्के खरीदें',
  buy_coins:'सिक्के खरीदें',


  //filters
  choose_desired_location_for_models:'उपलब्ध मॉडल की सूची प्राप्त करने के लिए वांछित शहर चुनें',
  cities:'शहरों',
  filters:'फिल्टर',
  change:'परिवर्तन',
  search_by_stars:'सितारों द्वारा खोजें',
  close:'बंद करे',


  //edit profile both
  edit_profile:'प्रोफाइल एडिट करें',
  name:'नाम',
  save:'बचाना',

  //contact us
  contact_us:'हमसे संपर्क करें',
  message_title:'संदेश का शीर्षक',
  description:'विवरण',
  model_member_complain:'मॉडल / सदस्य शिकायत',
  username:'उपयोगकर्ता नाम',
  model_member_noshow:'मॉडल / सदस्य कोई शो नहीं',

  model_accepted_bid:'मॉडल स्वीकृत बोली पूरी नहीं हुई',
  member_send_bid:'सदस्य बोली को पूरा न करें',
  bad_behaviour:'बुरा व्यवहार या हिंसा',
  model_paid_coin:'मॉडल पेड सिक्का',
  model_didnot_meet:'मॉडल से मुलाकात नहीं हुई',
  please_detail_complain:'कृपया अपनी शिकायत विस्तार से बताएं',
  objectional_content:'आपत्तिजनक सामग्री, दुर्व्यवहार या हिंसा',


  //buy coins and stars
  buy_coins:'सिक्के खरीदें',
  buy_stars:'सितारे खरीदें',
  buy_coins_to_unlock_conversation:'बातचीत अनलॉक करने और पहली तारीखों पर जाने के लिए सिक्के खरीदें!',
  choose_number_of_coins:'सिक्कों की संख्या चुनें.',
  buy_now:'अभी खरीदें',
  total_price:'कुल कीमत',
  buy_star_rating_visible_partner:'अपने भागीदारों के लिए अधिक दृश्यमान के लिए सितारों की रेटिंग खरीदें!',
  choose_number_of_stars:'सितारों की संख्या चुनें।',


//notifcaiton
  enter_raise_amount:'उठाएँ राशि दर्ज करें',
  enter_amount_dollars:'डॉलर में राशि दर्ज करें',
  raise:'उठाना',
  raised:'उठाया',
  accepted:'स्वीकृतैं',
  rejected:'अस्वीकृत',
  bid_price:'दाम लगाना',
  pending:'अपूर्ण',


  //cchat lit
  chat_list : "चैट सूची",


  //choose language
  choose_language:'भाषा चुनें',
  language:'भाषा',

  //drawer both
  home:'होम',
  chats:'चैट',
  coins:'सिक्के',
  stars:'सितारे',
  notifications:'सूचनाएं',
  update_profile:'प्रोफ़ाइल अपडेट करें',
  change_password:'पासवर्ड बदलें',
  legal:'कानूनी',
  privacy_policy:'गोपनीयता नीति',
  terms_and_condition:'नियम व शर्त',
  logout:'लोग आउट',
  bids:'बोलियां',

  //header
  lists : 'सूचियाँ',
  activity:'गतिविधि',


  //drawer
  coins:'सिक्के',
  stars:'सितारे',

  //location modal overlay
  choose:'चुनें',
  distance:'दूरी',
  choose_distance:'दूरी चुनें (किमी)',

  //block model
  block :'खंड',
  unblock:'अनब्लॉक',
  choose_block_reason:'ब्लॉक करने का कारण दर्ज करें (वैकल्पिक)',
  block_this_model:'इस मॉडल को ब्लॉक करें',

  //
  enter_otp_email:'कृपया हमें आपके द्वारा भेजे गए ओटीपी के साथ अपना ईमेल सत्यापित करें। इनबॉक्स में न होने पर जंक मेल की जाँच करें',

  //video
  add_video:'वीडियो जोड़ें',
  see_video:'देखें वीडियो',
  upload:'डालना',
  no_video_found:'कोई वीडियो नहीं मिला',


    //location
    send:'Send',
    your_current_location:'Your Current location :',
    nearby_favourite_places:'Nearby Favourite Places :',
    send_location:'Send Location',


};
