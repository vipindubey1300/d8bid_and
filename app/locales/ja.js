export default {
  //welcome
  welcome_sign_in: 'サインイン',
  welcome_sign_up: 'サインアップ',

//login
  email_id: '電子メールID',
  password:'パスワード',
  forgot_password:"パスワードをお忘れですか",
  login_with:'またはでログイン',
  dont_have_account:"アカウントを持っていません",
  sign_up:'サインアップ',
  login:'ログイン',

//forgot password
  enter_registered_email:'登録メールアドレスを入力',
  send_my_password:'パスワードを送信する',

//enter otp
  password_recovery: 'パスワードの復元',
  enter_otp:'OTPを入力',
  resend_otp:'OTPを再送信',

//otpshcnagepassword
  please_change_your_password:'パスワードを変更してください',
  new_password:'	新しいパスワード',
  confirm_password:'パスワードを認証する',
  change:'変化する',

  //location /location Socailamember
  profile_details:'プロフィール詳細',
  country:'	国',
  state:'	状態',
  city:'シティ',
  gender:'性別',
  interested_gender:'	興味のある性別',
  male:'男性',
  female:'	女性',
  transgender:'トランスジェンダー',


  //createprofiel /both
  create_profile:'プロフィール作成',
  add_images:'画像を追加',
  model_display_name:'モデル表示名',
  member_display_name:'	メンバー表示名',
  age:'年齢',
  height_cms:'身長（cm）',
  weight_kgs:'重量（キログラム）',
  about_bio:'バイオについて（オプション）',
  ethnicity:'人種',
  body_type:'ボディタイプ',
  save:'保存する',

  //choose member type both
  who_you_are:'あなたは誰ですか',
  please_select_type:'あなたのタイプを選択してください',
  enter_as_model:'モデルとして入力',
  enter_as_member:'会員になる',

  //signup member and sign up model
  app_user_name:'	アプリユーザー名',
  email:'	Eメール',
  phone_no:'	電話番号。',
  password:'パスワード',
  confirm_password:'パスワードを認証する',
  accept_terms_condition:'利用規約に同意する',
  or_signup_with:'	またはSignUp with',
  already_have_account :'すでにアカウントをお持ちですか',
  log_in:'ログイン',

  //instagram email
  enter_your_email:'あなたのEメールIDを入力してください',
  submit:'提出する',

  //homepasge model/member
  na:'N/A',
  height:'高さ',
  weight:'重量',
  cms:'cms',
  kgs:'キログラム',
  about:'約',
  available_now:'今すぐ入手可能',
  available_later:'後で利用可能',

  //profile_details
  model_profile:'モデルプロファイル',
  member_profile:'メンバープロフィール',
  set_bid_amount:'入札額を設定する',
  bid_now:'今すぐ入札',
  choose_bid_amount_dollars:'入札金額を選択する（US $）',
  select:'選択する',
  your_previous_bid_amount:'以前の入札金額',
  raised_bid_amount:'上げられた入札額',


  //reset password
  reset_password:'パスワードを再設定する',
  please_update_your_password:'パスワードを更新してください',
  old_password:'以前のパスワード',
  new_password:'新しいパスワード',
  confirm_password:'パスワードを認証する',
  reset:'リセット',

  //profile model
  profile:'プロフィール',
  edit:'編集する',
  location:'ロケーション',

  //payment
  payment_mode:'支払いモード',
  select_payment_mode:'支払い方法を選択',
  credit_debit:'クレジット/デビットカード',
  paypal:'ペイパル',
  credit_card:'クレジットカード',

  //model rating
  rate:'レート',

  //model profile chat.
  redeem_one_coin_chat:'1コインをチャットに引き換える',
  redeem_one_coin_chat_to_lock:'	1コインを引き換えて、承認された入札を確定し、モデルの個人連絡先の詳細を受け取る',
  redeem_now:'今すぐ交換',
  no_coins_left:'コインが残っていません',
  buy_coins_to_connect:'接続するためにコインを購入してください',
  buy_coins:'コインを買う',


  //filters
  choose_desired_location_for_models:'利用可能なモデルのリストを取得するために希望の都市を選択してください',
  cities:'都市',
  filters:'フィルター',
  change:'変化する',
  search_by_stars:'星で検索',
  close:'閉じる',

  //edit profile both
  edit_profile:'プロファイル編集',
  name:'名',
  save:'保存する',

  //contact us
  contact_us:'お問い合わせ',
  message_title:'メッセージタイトル',
  description:'説明',
  model_member_complain:'モデル/メンバーの苦情',
  username:'ユーザー名',
  model_member_noshow:'モデル/メンバーノーショー',
  objectional_content:'異論のあるコンテンツ、虐待または暴力',


  model_accepted_bid:'モデルの承認された入札単価が満たされなかった',
  member_send_bid:'メンバーが入札を満たしていない',
  bad_behaviour:'悪い行動または暴力',
  model_paid_coin:'モデル有料コイン',
  model_didnot_meet:'モデルは会いませんでした',
  please_detail_complain:'あなたの苦情を詳しく書いてください',

  //buy coins and stars
  buy_coins:'コインを買う',
  buy_stars:'星を購入する',
  buy_coins_to_unlock_conversation:'会話をロック解除し、最初の日に行くためにコインを購入！',
  choose_number_of_coins:'コインの数を選んでください。',
  buy_now:'今買う',
  total_price:'合計金額',
  buy_star_rating_visible_partner:'あなたのパートナーにもっと見えるように星評価を購入しましょう！',
  choose_number_of_stars:'星の数を選択してください。',


//notifcaiton
  enter_raise_amount:'引き上げ金額を入力',
  enter_amount_dollars:'金額をドルで入力',
  raise:'上げる',
  raised:'上げた',
  accepted:'受け入れ済み',
  rejected:'拒否されました',
  bid_price:'入札価格',
  pending:'保留中',

  //cchat lit
  chat_list : "チャットリスト",

  //choose language
  choose_language:'言語を選択',
  language:'言語',

  //drawer both
  home:'ホーム',
  chats:'チャット',
  coins:'	コイン',
  stars:'星',
  notifications:'通知',
  update_profile:'プロフィールを更新',
  change_password:'パスワードを変更する',
  legal:'法的',
  privacy_policy:'個人情報保護方針',
  terms_and_condition:'利用規約',
  logout:'ログアウト',
  bids:'入札単価',



  //header
  lists : 'リスト',
  activity:'アクティビティ',


  //drawer
  coins:'コイン',
  stars:'星',

  //location modal overlay
  choose:'選ぶ',
  distance:'距離',
  choose_distance:'距離を選択してください（km）',

  //block model
  block :'ブロック',
  unblock:'ブロック解除',
  choose_block_reason:'ブロックする理由を入力してください（オプション）',
  block_this_model:'このモデルをブロック',

  //
  enter_otp_email:'送信したOTPでメールを確認してください。受信トレイにない場合は迷惑メールを確認する',
  //video
  add_video:'ビデオを追加',
  see_video:'ビデオを見る',
  upload:'アップロードする',
  no_video_found:'ビデオが見つかりません',

    //location
    send:'Send',
    your_current_location:'Your Current location :',
    nearby_favourite_places:'Nearby Favourite Places :',
    send_location:'Send Location',

};
