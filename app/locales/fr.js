export default {
  //welcome
  welcome_sign_in: 'se connecter',
  welcome_sign_up: "S'inscrire",

//login
  email_id: 'Identifiant Email',
  password:'Mot de passe',
  forgot_password:"Mot de passe oublié",
  login_with:'ou connectez-vous avec',
  dont_have_account:"Je n'ai pas de compte",
  sign_up:"S'inscrire",
  login:"S'identifier",

//forgot password
  enter_registered_email:'Entrez un email enregistré',
  send_my_password:'Envoyer mon mot de passe',

//enter otp
  password_recovery: 'Récupération de mot de passe',
  enter_otp:'Entrez OTP',
  resend_otp:'Renvoyer OTP',

//otpshcnagepassword
  please_change_your_password:'Veuillez changer votre mot de passe',
  new_password:'nouveau mot de passe',
  confirm_password:'Confirmez le mot de passe',
  change:'Changement',

  //location /location Socailamember
  profile_details:'Détails du profil',
  country:'Pays',
  state:'Etat',
  city:'Ville',
  gender:'Le sexe',
  interested_gender:'Sexe intéressé',
  male:'Mâle',
  female:'Femelle',
  transgender:'transgenres',


  //createprofiel /both
  create_profile:'Créer un profil',
  add_images:'Ajouter des images',
  model_display_name:"Nom d'affichage du modèle",
  member_display_name:"Nom d'affichage du membre",
  age:'Âge',
  height_cms:'Hauteur (en cm)',
  weight_kgs:'Poids (en kg)',
  about_bio:'À propos de la bio (facultatif)',
  ethnicity:'Ethnicité',
  body_type:'Type de corps',
  save:'sauvegarder',

  //choose member type both
  who_you_are:'Qui tu es',
  please_select_type:'Veuillez choisir votre type',
  enter_as_model:'Entrez comme modèle',
  enter_as_member:'Entrez en tant que membre',

  //signup member and sign up model
  app_user_name:"Nom d'utilisateur de l'application",
  email:'Email',
  phone_no:'Pas de téléphone.',
  password:'Mot de passe',
  confirm_password:'Confirmez le mot de passe',
  accept_terms_condition:'Accepter les termes et conditions',
  or_signup_with:"ou s'inscrire avec",
  already_have_account :'Vous avez déjà un compte',
  log_in:"s'identifier",

  //instagram email
  enter_your_email:'Entrez votre identifiant',
  submit:'Soumettre',

  //homepasge model/member
  na:'N/A',
  height:'la taille',
  weight:'Poids',
  cms:'cms',
  kgs:'kgs',
  about:'Sur',
  available_now:'Disponible dès maintenant',
  available_later:'Disponible plus tard',

  //profile_details
  model_profile:'Profil de modèle',
  member_profile:'Profil de membre',
  set_bid_amount:'Définir le montant de la soumission',
  bid_now:'Enchérir maintenant',
  choose_bid_amount_dollars:'Choisissez le montant de la soumission ( US $)',
  select:'Sélectionner',
  your_previous_bid_amount:'Le montant de votre offre précédente',
  raised_bid_amount:"Montant de l'offre augmentée'",


  //reset password
  reset_password:'réinitialiser le mot de passe',
  please_update_your_password:'Veuillez mettre à jour votre mot de passe',
  old_password:'ancien mot de passe',
  new_password:'nouveau mot de passe',
  confirm_password:'Confirmez le mot de passe',
  reset:'Réinitialiser',

  //profile model
  profile:'Profil',
  edit:'modifier',
  location:'Emplacement',

  //payment
  payment_mode:'Mode de paiement',
  select_payment_mode:'Sélectionnez le mode de paiement',
  credit_debit:'Carte de crédit / débit',
  paypal:'Pay Pal',
  credit_card:'Carte de crédit',

  //model rating
  rate:'Taux',

  //model profile chat.
  redeem_one_coin_chat:'Échangez 1 pièce pour discuter',
  redeem_one_coin_chat_to_lock:'Échangez 1 pièce pour bloquer les offres acceptées et recevoir les coordonnées privées des modèles',
  redeem_now:'Échangez maintenant',
  no_coins_left:'Pas de pièces à gauche',
  buy_coins_to_connect:'Veuillez acheter des pièces pour vous connecter',
  buy_coins:'Acheter des pièces',


  //filters
  choose_desired_location_for_models:'Choisissez les villes souhaitées pour obtenir la liste des modèles disponibles',
  cities:'Villes',
  filters:'Les filtres',
  change:'Changement',
  search_by_stars:'Recherche par étoiles',
  close:'proche',


  //edit profile both
  edit_profile:'Editer le profil',
  name:'prénom',
  save:'sauvegarder',

  //contact us
  contact_us:'Contactez nous',
  message_title:'Titre du message',
  description:'La description',
  model_member_complain:'Modèle / membre se plaint',
  username:"Nom d'utilisateur",
  model_member_noshow:'Modèle / Membre No Show',
  

  model_accepted_bid:"Le modèle accepté n'a pas été accepté",
  member_send_bid:'Les membres ont envoyé une offre ne se sont pas rencontrés',
  bad_behaviour:'Mauvais comportement ou violence',
  model_paid_coin:'Modèle payé pièce',
  model_didnot_meet:"Le modèle ne s'est pas rencontré",
  please_detail_complain:'Veuillez détailler votre plainte',
  objectional_content:"Contenu d'objection, abus ou violence",


  //buy coins and stars
//  buy_coins:'Buy Coins',
  buy_stars:'Acheter des étoiles',
  buy_coins_to_unlock_conversation:'Achetez des pièces pour déverrouiller les conversations et commencer les premières dates!',
  choose_number_of_coins:'Choisissez le nombre de pièces.',
  buy_now:'Acheter maintenant',
  total_price:'Prix ​​total',
  buy_star_rating_visible_partner:'Acheter des notes étoiles pour plus visible pour vos partenaires!',
  choose_number_of_stars:"Choisissez le nombre d'étoiles.",

  //notifcaiton
    enter_raise_amount:'Entrez le montant de la relance',
    enter_amount_dollars:'Entrez le montant en dollars',
    raise:'Élever',
    raised:'Surélevé',
    accepted:'Accepté',
    rejected:'Rejeté',
    bid_price:"Prix ​​de l'offre",
    pending:'En attendant',

    //cchat lit
  chat_list : "Listes de discussion",

  //choose language
  choose_language:'Choisissez la langue',
  language:'La langue',

  //drawer both
  home:'Accueil',
  chats:'Chats',
  coins:'Pièces de monnaie',
  stars:'Étoiles',
  notifications:'Les notifications',
  update_profile:'Mettre à jour le profil',
  change_password:'Changer le mot de passe',
  legal:'Légal',
  privacy_policy:'Politique de confidentialité',
  terms_and_condition:'Termes et conditions',
  logout:'Connectez - Out',
  bids:'Les offres',

   //header
   lists : 'Des listes',
   activity:'Activité',

   //drawer
   coins:'Pièces de monnaie',
   stars:'Étoiles',

   //location modal overlay
   choose:'Choisissez la',
   distance:'Distance',
   choose_distance:'Choisissez la distance (km)',

   //block model
   block :'Bloc',
   unblock:'Débloquer',
   choose_block_reason:'Entrez le motif de blocage (facultatif)',
   block_this_model:'Bloquer ce modèle',

   //
   enter_otp_email:"S'il vous plaît vérifier votre email avec OTP, nous vous avons envoyé. Vérifier le courrier indésirable s'il n'est pas dans la boîte de réception",
 
  //video
  add_video:'Ajouter une vidéo',
  see_video:'Voir la vidéo',
  upload:'Télécharger',
  no_video_found:'Aucune vidéo trouvée',

    //location
    send:'Send',
    your_current_location:'Your Current location :',
    nearby_favourite_places:'Nearby Favourite Places :',
    send_location:'Send Location',


};
