import React, {Component} from 'react';
import {Text, View, Image,Dimensions,Alert,ToastAndroid,StatusBar,BackHandler,StyleSheet,ImageBackground,TouchableOpacity} from 'react-native';
import { colors} from './Variables';
import { withNavigationFocus } from 'react-navigation';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';



class Welcome extends React.Component {
    constructor(props) {
        super(props);

    }



  //1
 checkPermission(){
   // ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);

   

        firebase.messaging().hasPermission()
        .then(enabled => {
          if (enabled) {
            // user has permissions
            this.getToken()
          } else {
            // user doesn't have permission
            this.requestPermission()
          } 
        });
    
  }
  
    //3
  getToken= async () => {
   // ToastAndroid.show("Gettign token....",ToastAndroid.SHORT);
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            // user has a device token
            //ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
        }
        else{
          //ToastAndroid.show("no toeke.....",ToastAndroid.SHORT);
        }
    }
  }
  
    //2
   requestPermission(){

        firebase.messaging().requestPermission()
      .then(() => {
        // User has authorised  
        this.getToken();
      })
      .catch(error => {
        // User has rejected permissions  
        ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);
      });
        
  }
  
  handleBackWithAlert = () => {
  
		if (this.props.isFocused) {

            Alert.alert(
            'Exit App',
            'Exiting the application?',
            [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel'
            },
            {
              text: 'OK',
              onPress: () => BackHandler.exitApp()
            }
            ],
            {
            cancelable: false
            }
          );
          

	return true;
}
}



componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });



	BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);

}

componentWillUnmount() {
   this.backhandler = BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
  //  this.backhandler.remove()
   //ToastAndroid.show("ASdf",ToastAndroid.LONG)

}


  
    render() {
        return (
         <ImageBackground
         source={require('../assets/welcome.jpg')}
         style={{resizeMode:'stretch',height:'100%'}}>
         <View 
         style={styles.container}>

                    <TouchableOpacity
                     style={styles.login_btn}
                     onPress={()=> this.props.navigation.navigate("Login")}
                    >
                    <Text style={styles.login_text}>{I18n.t('welcome_sign_in')}</Text>

                    </TouchableOpacity>


                    <TouchableOpacity
                    style={styles.signup_btn}
                    onPress={()=> this.props.navigation.navigate("MemberType")}
                    >
                        <Text style={styles.signup_text}>{I18n.t('welcome_sign_up')}</Text>
                    </TouchableOpacity>


         </View>

         </ImageBackground>

        )
    }
}

let styles = StyleSheet.create({
  container:{
    position:'absolute',
    alignItems: 'center',
    bottom:40,
    left:0,
    right:0,
    width:'100%',
    height:'20%',
    flexDirection:'row',
    
  },
  login_btn:{
     flex:1,
     margin:10,
     width:"70%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_secondary,
     borderRadius:10,
     borderWidth: 1,
     borderColor: '#fff',
   
    
  },
  login_text:{
    color:colors.color_primary,
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  signup_btn:{
    flex:1,
    margin:10,
    width:"70%",
    justifyContent:'center',
     alignItems:'center',
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  
   
 },
 signup_text:{
   color:colors.color_secondary,
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
  

}
)

export default withNavigationFocus(Welcome);
