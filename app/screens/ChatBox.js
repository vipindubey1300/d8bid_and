import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,ToastAndroid,
  StatusBar,ScrollView,Keyboard,StyleSheet,FlatList,Platform,
  KeyboardAvoidingView,TouchableWithoutFeedback,
  BackHandler,TextInput,ImageBackground,TouchableOpacity
  ,ActivityIndicator,Animated,Clipboard,Linking} from 'react-native';
import { colors,fonts,urls } from './Variables';
import {Card} from 'native-base';
import { withNavigationFocus } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import { Slider,Overlay } from 'react-native-elements';

import {Header} from 'react-native-elements';

 class ChatBox extends Component {
    constructor(props) {
        super(props);
         this.paddingInput = 0;
        this.state ={
          loading_status:false,

           user_image:null,
           user_name:'',
           user_id:0,
           message:'',
           chats:[],
           flinks:[],
           active_flinks:[],
           flink_status : false,
           show_flings:true,
           bottom:0,
           redeem_modal:false,
           block_status:false,
           block:0,
           whoblock:'',
           type_status:false,
           other_type:false



                  };

    }


    copyText(text){
      Clipboard.setString(text)
       Platform.OS === 'android'
                            ?   ToastAndroid.show('Copied to clipboard', ToastAndroid.SHORT)
                            : Alert.alert("Copied to clipboard")

    }

    seeLocation = (addr,lat,lng) => {
      console.log('open directions')
     let f = Platform.select({
          ios: () => {
              Linking.openURL('http://maps.apple.com/maps?daddr='+lat+','+lng);
          },
          android: () => {
              console.log('ANDROID')
              Linking.openURL('http://maps.apple.com/maps?daddr='+lat+','+lng).catch(err => console.error('An error occurred', err));;
          }
      });

      f();
  }


    seeLocation(addr,lat,lng){
      var url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=Los+Angeles";
      Linking.canOpenURL(url).then(supported => {
    if (!supported) {
        console.log('Can\'t handle url: ' + url);
    } else {
        return Linking.openURL(url);
    }
}).catch(err => console.error('An error occurred', err));
    }

    fetchFlinks(){
     // ToastAndroid.show('flingsssss fetch',ToastAndroid.LONG)
     // this.setState({loading_status:true})
                     let url = urls.base_url +'api/flink_list'

                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },


                        }).then((response) => response.json())
                              .then((responseJson) => {
                             // this.setState({loading_status:false,message:''})
                 //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                                   if(!responseJson.error){

                                    var length = responseJson.result.length.toString();

                                    var temp_arr=[]
                                    for(var i = 0 ; i < length ; i++){

                                          var id = responseJson.result[i].id
                                          var name = responseJson.result[i].name
                                          var image = responseJson.result[i].image
                                          var status = responseJson.result[i].status






                                                const array = [...temp_arr];
                                                array[i] = { ...array[i], id: id };
                                                array[i] = { ...array[i], name:name };
                                                array[i] = { ...array[i], image:image };
                                                array[i] = { ...array[i], status:status };




                                                temp_arr = array

                                    }
                                    this.setState({ flinks : temp_arr});



                                }
                                else{

                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                Platform.OS === 'android'
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });


    }

    fetchActiveFlinks(){

    //  ToastAndroid.show('flingsssss fetch  active',ToastAndroid.SHORT)

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();

         var text = this.state.message
         formData.append('user_id', item);//our id
         formData.append('model_id',this.state.user_id);

        //  formData.append('user_id', 801);//our id
        //  formData.append('model_id',800);

        // ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)
           let url = urls.base_url +'api/perchased_flink'
          fetch(url, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
            },

            body:formData
          }).then((response) => response.json())
                .then((responseJson) => {
               // this.setState({loading_status:false,message:''})
   //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                     if(!responseJson.error){
                     // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                      var length = responseJson.result.length.toString();

                      var temp_arr=[]
                      for(var i = 0 ; i < length ; i++){

                            var id = responseJson.result[i].flinks.id
                            var name = responseJson.result[i].flinks.name
                            var image = responseJson.result[i].flinks.image
                            var status = responseJson.result[i].flinks.status






                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name:name };
                                  array[i] = { ...array[i], image:image };
                                  array[i] = { ...array[i], status:status };




                                  temp_arr = array

                      }
                      this.setState({ active_flinks : temp_arr});


                      this.makeFLings()
                       // ToastAndroid.show(JSON.stringify(this.state.active_flinks),ToastAndroid.LONG)


                  }
                  else{

                    ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                  }
                }).catch((error) => {
                  this.setState({loading_status:false})

                  // Platform.OS === 'android'
                  // ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                  // : Alert.alert("Connection Error !")
                });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

      // this.setState({loading_status:true})



     }





     fetchMessage(){
     // ToastAndroid.show("fetching",ToastAndroid.LONG)
    // ToastAndroid.show('feetchhh meessage',ToastAndroid.SHORT)
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();


//ToastAndroid.show(item+'...'+this.state.user_id,ToastAndroid.LONG)


          formData.append('form_id', item);//our id
           formData.append('to_id',this.state.user_id);
           formData.append('type_status',this.state.type_status ? 1 : 0);
          //formData.append('form_id', 1096);
          //formData.append('to_id',961);



                          // this.setState({loading_status:true})
                          let url = urls.base_url +'api/chat_message'

                        //  / ToastAndroid.show(url+".."+JSON.stringify(formData),ToastAndroid.LONG)

                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                             // this.setState({loading_status:false,message:this.state.message})
                // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)

                          console.log("Ressss",responseJson)
                                   if(!responseJson.error){

                                 

                                    

                                    //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.SHORT)

                                   // ToastAndroid.show(JSON.stringify(responseJson.type_status), ToastAndroid.SHORT)

                                                                    // Platform.OS === 'android'
                                                                    // ?   ToastAndroid.show(JSON.stringify(responseJson.type_status), ToastAndroid.SHORT)
                                                                    // : Alert.alert(JSON.stringify(responseJson.type_status))

                                     var block = responseJson.block
                                     var whoblock = responseJson.whoblock

                                     var other_type = responseJson.type_status
                                    this.setState({
                                      other_type:other_type
                                    })


                               // ToastAndroid.show(JSON.stringify(responseJson.whoblock),ToastAndroid.LONG)

                                    var length = responseJson.Chat.length.toString();

                                    var temp_arr=[]
                                    for(var i = 0 ; i < length ; i++){

                                          var chat_id = responseJson.Chat[i].id
                                          var form_id = responseJson.Chat[i].form_id
                                          var to_id = responseJson.Chat[i].to_id
                                          var message = responseJson.Chat[i].message
                                          var attachment = responseJson.Chat[i].attachment
                                          var flinks = responseJson.Chat[i].flinks
                                          var reading_status = responseJson.Chat[i].reading_status
                                          var created_at = responseJson.Chat[i].created_at

                                          var address = responseJson.Chat[i].address
                                          var lat = responseJson.Chat[i].lat
                                          var lng = responseJson.Chat[i].lng
                                          var sent_flag = true
                                          if(form_id == item){

                                          }
                                          else{
                                            sent_flag = false
                                          }

                                                const array = [...temp_arr];
                                                array[i] = { ...array[i], chat_id: chat_id };
                                                array[i] = { ...array[i], sent_flag:sent_flag };
                                                array[i] = { ...array[i], message:message };
                                                array[i] = { ...array[i], attachment:attachment };
                                                array[i] = { ...array[i], flinks:flinks };
                                                array[i] = { ...array[i], reading_status:reading_status };
                                                array[i] = { ...array[i], created_at:created_at };
                                                array[i] = { ...array[i], address:address };
                                                array[i] = { ...array[i], lat:lat };
                                                array[i] = { ...array[i], lng:lng };




                                                temp_arr = array

                                    }

                                    {
                                      // ToastAndroid.show(this.state.chats.length.toString(),ToastAndroid.SHORT)
                                      //  
                                     }

                                    //1 or 0 means blocked ...2 means not blocked

                                  //  ToastAndroid.show(text,ToastAndroid.LONG)
                                    this.setState({ chats : temp_arr,
                                      block_status: block == 1 || block == 0 ? true : false,
                                      whoblock:whoblock});

                            

                                }
                                else{

                                  var block = responseJson.block
                                  var whoblock = responseJson.whoblock
                                  this.setState({block_status:block == 1 || block == 0 ? true : false,
                                  whoblock:whoblock});

                                  //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                // Platform.OS === 'android'
                                // ?   ToastAndroid.show('gefetch message', ToastAndroid.SHORT)
                                // : Alert.alert("Connection Error !")

                                //ToastAndroid.show(error.message, ToastAndroid.SHORT)
                              });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });


    }

    purchaseFlings(gif,status,id){

      //ToastAndroid.show('flingsssss purchase',ToastAndroid.SHORT)

   // ToastAndroid.show("shiow", ToastAndroid.SHORT);
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();

         var text = this.state.message
          formData.append('user_id', item);//our id
          formData.append('model_id',this.state.user_id);
          formData.append('flink_id', id);
          //formData.append('coins', 1);
          this.setState({loading_status:true})
         //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
                          // this.setState({loading_status:true})
                          let url = urls.base_url +'api/flink_buy'

                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})
                             // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                   if(!responseJson.error){

                                     //this.props.navigation.navigate("HomeScreen");
                                     AsyncStorage.getItem( 'coins' )
                                     .then( data => {

                                       // the string value read from AsyncStorage has been assigned to data
                                       console.log( data );
                                         var c = parseInt(data)
                                         c= c - 1

                                       //save the value to AsyncStorage again
                                       AsyncStorage.setItem( 'coins', c.toString() );

                                     }).done();
                                     Platform.OS == 'android'
                                     ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                     : Alert.alert(responseJson.message)


                                  var l = this.state.flinks.length
                                  for(var i = 0 ; i < l ; i++){

                                      if(this.state.flinks[i].id == id){
                                        //status 0 means true....byued...theflinkss
                                        let flink = [...this.state.flinks];
                                        flink[i].status = 0;
                                        this.setState({flinks: flink},);

                                      }

                                  }


                                }
                                else{

                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                 Platform.OS === 'android'
                                ?   ToastAndroid.show('Connection Error !', ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });


    }




    checkBlockStatusFlings(gif){
      if(this.state.block_status == false){
        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
            var formData = new FormData();

           var text = this.state.message
           formData.append('form_id', item);//our id
           formData.append('to_id',this.state.user_id);
            formData.append('flinks',gif);


                            this.setState({loading_status:true})
                            let url = urls.base_url +'api/chat'
                            fetch(url, {
                            method: 'POST',
                            headers: {
                              'Accept': 'application/json',
                              'Content-Type': 'multipart/form-data',
                            },
                        body: formData

                          }).then((response) => response.json())
                                .then((responseJson) => {
                                this.setState({flink_status:false,message:'',loading_status:false})

                                     if(!responseJson.error){

                                     // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                    var l = this.state.chats.length

                                    const array = [...this.state.chats];
                                    array[l] = { ...array[l], chat_id: 0 };
                                    array[l] = { ...array[l], sent_flag:true };
                                    array[l] = { ...array[l], message:null };
                                    array[l] = { ...array[l], attachment:null };
                                    array[l] = { ...array[l], flinks:gif };
                                    array[l] = { ...array[l], address:null };
                                    array[l] = { ...array[l], lat:null };
                                    array[l] = { ...array[l], lng:null };

                                    array[l] = { ...array[l], reading_status:0 };
                                    array[l] = { ...array[l], created_at:"2019-11-28 00:00:00" };




                                         this.setState({ chats : array});




                                  }
                                  else{

                                    ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                  }
                                }).catch((error) => {
                                  this.setState({loading_status:false})

                                  Platform.OS === 'android'
                                  ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                  : Alert.alert("Connection Error !")
                                });
          }
          else{
            ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });
      }
      else{
        Platform.OS === 'android'
        ?   ToastAndroid.show(this.state.whoblock, ToastAndroid.SHORT)
        : Alert.alert(this.state.whoblock)
      }
    }



    sendFlings(gif,status,flink_id){

     // ToastAndroid.show('flingsssss sendddddd',ToastAndroid.SHORT)
     // ToastAndroid.show(gif, ToastAndroid.SHORT);
     if(status == 1){
        //purchase flings
        this.purchaseFlings(gif,status,flink_id)


     }else{
     // ToastAndroid.show("sendd", ToastAndroid.SHORT);
          this.checkBlockStatusFlings(gif)

     }


    }

    sendAttachment(image){
      // ToastAndroid.show(gif, ToastAndroid.SHORT);
       AsyncStorage.getItem("user_id").then((item) => {
         if (item) {
           var formData = new FormData();

          var text = this.state.message
          formData.append('form_id', item);//our id
           formData.append('to_id',this.state.user_id);
           formData.append('attachment',image);
           this.setState({loading_status:true})





                           // this.setState({loading_status:true})
                           let url = urls.base_url +'api/chat'
                           fetch(url, {
                           method: 'POST',
                           headers: {
                             'Accept': 'application/json',
                             'Content-Type': 'multipart/form-data',
                           },
                       body: formData

                         }).then((response) => response.json())
                               .then((responseJson) => {
                             //  this.setState({flink_status:false,message:''})
                         //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                           this.setState({loading_status:false})
                                    if(!responseJson.error){


                                      var l = this.state.chats.length

                                      const array = [...this.state.chats];
                                      array[l] = { ...array[l], chat_id: 0 };
                                      array[l] = { ...array[l], sent_flag:true };
                                      array[l] = { ...array[l], message:null };
                                      array[l] = { ...array[l], attachment:image };
                                      array[l] = { ...array[l], flinks:null};
                                      array[l] = { ...array[l], address:null };
                                      array[l] = { ...array[l], lat:null };
                                      array[l] = { ...array[l], lng:null };

                                      array[l] = { ...array[l], reading_status:0 };
                                      array[l] = { ...array[l], created_at:"2019-11-28 00:00:00" };





                                           this.setState({ chats : array});





                                 }
                                 else{

                                   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                 }
                               }).catch((error) => {
                                 this.setState({loading_status:false})

                                  Platform.OS === 'android'
                                ?   ToastAndroid.show('Connection Error !', ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                               });
         }
         else{
           ToastAndroid.show("User not found !",ToastAndroid.LONG)
         }
       });


     }

     sendLocation(addr,lat,lng){

      // ToastAndroid.show('message send',ToastAndroid.SHORT)


       AsyncStorage.getItem("user_id").then((item) => {
         if (item) {
           var formData = new FormData();

          var text = this.state.message
           formData.append('form_id', item);//our id
           formData.append('to_id',this.state.user_id);
           formData.append('address', addr);
           formData.append('lat', lat);
           formData.append('lng', lng);

           var l = this.state.chats.length

           const array = [...this.state.chats];
           array[l] = { ...array[l], chat_id: 0 };
           array[l] = { ...array[l], sent_flag:true };
           array[l] = { ...array[l], message:null };
           array[l] = { ...array[l], flinks:null };
           array[l] = { ...array[l], attachment:null };
           array[l] = { ...array[l], address:addr };
           array[l] = { ...array[l], lat:lat };
           array[l] = { ...array[l], lng:lng };

           array[l] = { ...array[l], reading_status:0 };
           array[l] = { ...array[l], created_at:"2019-11-28 00:00:00" };





          this.setState({ chats : array});


                           // this.setState({loading_status:true})
                           let url = urls.base_url +'api/chat'


                           fetch(url, {
                           method: 'POST',
                           headers: {
                             'Accept': 'application/json',
                             'Content-Type': 'multipart/form-data',
                           },
                       body: formData

                         }).then((response) => response.json())
                               .then((responseJson) => {
                               this.setState({loading_status:false,message:''})
                              // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                                    if(!responseJson.error){
 //this was prev working code
                                 //   var l = this.state.chats.length

                                 //   const array = [...this.state.chats];
                                 //   array[l] = { ...array[l], chat_id: 0 };
                                 //   array[l] = { ...array[l], sent_flag:true };
                                 //   array[l] = { ...array[l], message:text };
                                 //   array[l] = { ...array[l], flinks:null };
                                 //   array[l] = { ...array[l], attachment:null };




                                 //  this.setState({ chats : array});




                                 }
                                 else{

                                  // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                 }
                               }).catch((error) => {
                                 this.setState({loading_status:false})
                                  Platform.OS === 'android'
                                 ?   ToastAndroid.show('Connection Error !', ToastAndroid.SHORT)
                                 : Alert.alert("Connection Error !")
                               });
         }
         else{
           ToastAndroid.show("User not found !",ToastAndroid.LONG)
         }
       });



     }
    sendMessage(){

     // ToastAndroid.show('message send',ToastAndroid.SHORT)

     if(this.state.message.length > 0){
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();

         var text = this.state.message
          formData.append('form_id', item);//our id
          formData.append('to_id',this.state.user_id);
          formData.append('message', this.state.message);
          this.setState({message:''})
          //to unfocus text input
          this._textInput.blur();
          var l = this.state.chats.length

          const array = [...this.state.chats];
          array[l] = { ...array[l], chat_id: 0 };
          array[l] = { ...array[l], sent_flag:true };
          array[l] = { ...array[l], message:text };
          array[l] = { ...array[l], flinks:null };
          array[l] = { ...array[l], attachment:null };
          array[l] = { ...array[l], address:null };
          array[l] = { ...array[l], lat:null };
          array[l] = { ...array[l], lng:null };

          array[l] = { ...array[l], reading_status:0 };
          array[l] = { ...array[l], created_at:"2019-11-28 00:00:00" };




         this.setState({ chats : array});


                          // this.setState({loading_status:true})
                          let url = urls.base_url +'api/chat'


                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})
                             // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                                   if(!responseJson.error){
//this was prev working code
                                //   var l = this.state.chats.length

                                //   const array = [...this.state.chats];
                                //   array[l] = { ...array[l], chat_id: 0 };
                                //   array[l] = { ...array[l], sent_flag:true };
                                //   array[l] = { ...array[l], message:text };
                                //   array[l] = { ...array[l], flinks:null };
                                //   array[l] = { ...array[l], attachment:null };




                                //  this.setState({ chats : array});




                                }
                                else{

                                 // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                 Platform.OS === 'android'
                                ?   ToastAndroid.show('Connection Error !', ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
     }


    }

    selectPhotoTapped() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled photo picker');
          //ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg",
          };



        this.sendAttachment(photo)



          // this.setState({
          //   imageSource: source ,
          //   imageData: response.data,
          //   photo:photo,

          // });


          {/*photo is a file object to send to parameters */}
        }
      });
   }


    handleBackWithAlert = () => {
      // const parent = this.props.navigation.dangerouslyGetParent();
        // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
      // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)

      if (this.props.isFocused) {
        this.setState({type_status:false})

      //ToastAndroid.show('hsdfsjdhf',ToastAndroid.LONG)

     

                if(this.state.flink_status){
                       this.setState({flink_status:false,type_status:false})
                }

              else{
                    //   Alert.alert(
                    //   'Exit App',
                    //   'Exiting the application?',
                    //   [
                    //   {
                    //     text: 'Cancel',
                    //     onPress: () => console.log('Cancel Pressed'),
                    //     style: 'cancel'
                    //   },
                    //   {
                    //     text: 'OK',
                    //     onPress: () => BackHandler.exitApp()
                    //   }
                    //   ],
                    //   {
                    //   cancelable: false
                    //   }
                    // );
                    
                    this.props.navigation.goBack()
              }

    return true;
    }
    }

  //   keyboardWillShow = () => {
  //           alert('Keyboard Shown');
  //      this.setState({bottom:Dimensions.get('window').height * 0.38})
  //      };


  //      keyboardDidShow = () => {
  //       Alert.alert('Keyboard Shown');
  //  this.setState({bottom:Dimensions.get('window').height * 0.38})
  //  };



  //    keyboardWillHide = (event) => {
  //        alert('Keyboard Hidden');
  //    this.setState({bottom:0})
  //    };



  _keyboardDidShow = () => {
   // alert('Keyboard Shown');
    this.setState({bottom:Platform.OS == 'android'? 0 :
      Dimensions.get('window').height * 0.38})


      //this.myFlatListRef.scrollToIndex(1)

     
  }

  _keyboardDidHide = () => {
    //alert('Keyboard Hidden');
    this.setState({bottom:0})
  }




   async componentWillMount() {

       this.fetchMessage()

      await this.fetchFlinks()
      await this.fetchActiveFlinks()

    


      //   this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', 
      // this.keyboardWillShow );
      //  this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);


      this.keyboardDidShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardDidShow,
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardDidHide,
      );



    BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);
    //this.props.navigation.dispatch(DrawerActions.closeDrawer());
    this.props.navigation.closeDrawer();

    }

     componentWillUnmount() {

      //this.setState({type_status:false})
    // this.keyboardWillShowSub.remove();
    // this.keyboardWillHideSub.remove();

    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

    

    //to unfocus text input
    //this._textInput.blur();

    //ToastAndroid.show("AS",ToastAndroid.LONG)


     BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
     clearInterval(this.timer)

    }


    makeFLings(){


      var length = this.state.flinks.length.toString();
      if(this.state.active_flinks.length > 0){
        // /ToastAndroid.show("GH",ToastAndroid.LONG)
        for(var i = 0 ; i < length ; i++){
          for(var j = 0 ; j < this.state.active_flinks.length.toString() ; j++){
            if(this.state.flinks[i].id == this.state.active_flinks[j].id){
              //status 0 means true....byued...theflinkss
              let flink = [...this.state.flinks];
              flink[i].status = 0;
              this.setState({flinks: flink},);

            }
          }
        }
      }


    }

     



      checkBlockStatusLocation(){
        if(this.state.block_status == false){
          this.sendMessage()
        }
        else{
          Platform.OS === 'android'
          ?   ToastAndroid.show(this.state.whoblock, ToastAndroid.SHORT)
          : Alert.alert(this.state.whoblock)
        }
      }


      checkBlockStatus(){
        if(this.state.block_status == false){
          this.sendMessage()
        }
        else{
          Platform.OS === 'android'
          ?   ToastAndroid.show(this.state.whoblock, ToastAndroid.SHORT)
          : Alert.alert(this.state.whoblock)
        }
      }



      checkBlockStatusPhoto(){
        if(this.state.block_status == false){
          this.selectPhotoTapped()
        }
        else{
          Platform.OS === 'android'
          ?   ToastAndroid.show(this.state.whoblock, ToastAndroid.SHORT)
          : Alert.alert(this.state.whoblock)
        }
      }


    


//means this was started y model to chat
     async componentDidMount(){



        // setTimeout(() => {
        //   this.myFlatListRef.scrollToEnd({animated:true})
        // },1000);


        var result  = this.props.navigation.getParam('result')
      // ToastAndroid.show("ID>>>>"+JSON.stringify(result),ToastAndroid.LONG)
        this.setState({
          user_id : result['to_id'],
          user_name : result['name'],
          user_image : result['image'],

        })

        AsyncStorage.getItem('roles_id')
        .then((item) => {
                  if (item) {
                        if(item == 2){
                          //means model
                          this.setState({show_flings:false})
                        }
                        else{
                          this.setState({show_flings:true})
                        }
                }
                else {
                  ToastAndroid.show("Error !")
                  }
        });



    this.timer = setInterval(()=> this.fetchMessage(), 4000)
    // this.fetchMessage()
    //


  }



	// next(model_id){
	// 	if(this.state.models.length > 0){
	// 		//var jobs = this.state.jobs
	// 		for(var i = 0 ; i < this.state.models.length ; i++){
	// 			if(this.state.models[i].key == model_id){
	// 				result={}
	// 				result["model_id"] = this.state.models[i].id

	// 				this.props.navigation.navigate("ProfileDetails",{result : result});
	// 			}
	// 		}
	// 	}
  // }


  next(){

		if(this.state.show_flings){
     // ToastAndroid.show("FDSfsdfsdf",ToastAndroid.LONG)
      result= {}
      result["model_id"] = this.state.user_id
      this.props.navigation.navigate("ModelRating",{result:result})
    }
    else{
      //go to profile member
      let obj ={
        "member_id":this.state.user_id
      }
      this.props.navigation.navigate("MemberProfileBlock",{result:obj})
    }
  }

  showDialog(a,b,c,d){

    this.setState({redeem_modal:true})


    return(
      <View>

      </View>
    )
  }


  renderItem = ({item}) =>  {

   if(item.sent_flag){
      if(item.message != null ){
        return (

          <TouchableOpacity onLongPress={()=> this.copyText(item.message)}>
          <View style={{width:Dimensions.get('window').width * 0.7,margin:3,alignSelf:'flex-end'}}>

                  <View style={{ backgroundColor:'#d0d0d0',width:null,height:null,
                  alignSelf:'flex-end',flexDirection:'row',alignItems:'center',borderRadius:5}}>

                    <Text style={{fontSize:15,marginRight:4,color:'black'
                   ,padding:10,width:'auto',
                    alignSelf:'flex-start',borderRadius:5,alignSelf:'flex-end'}}>{item.message}</Text>

                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10}}>

                    <Text style={{marginRight:10,fontSize:12}}>{item.created_at.toString().substring(11,16)}</Text>

                    {
                      item.reading_status ==  0
                      ? <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                      : <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                    }

                    </View>

                    </View>

          </View>
          </TouchableOpacity>
        );
      }

      else  if(item.flinks != null ){

        return (

          <View style={{width:'100%',margin:3,height:null,padding:5}}>

          <View style={{width:'70%',height:null,padding:5,alignSelf:'flex-end'}}>
          <TouchableWithoutFeedback>
            <View style={{width:'100%',justifyContent:'space-between'}}>
                <View></View>

            <Image style={{height:120,width:120,alignSelf:'flex-end'}} source={{uri: urls.base_url+item.flinks}} />


            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10,marginBottom:10,marginTop:10,alignSelf:'flex-end'}}>

                    <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                    {
                      item.reading_status ==  0
                      ? <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                      : <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                    }

                    </View>



            </View>




              </TouchableWithoutFeedback>
              </View>

          </View>
        );

      }

      else if(item.address != null){
        return (


          <TouchableWithoutFeedback onPress={()=> this.seeLocation(item.address,item.lat,item.lng)}>

            <View style={{width:null,padding:10,alignSelf:'flex-end',backgroundColor:'white',margin:5}}>


            <Image style={{width:60, height: 60,margin:10}}  source={require('../assets/location-view.png')} />

            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10,marginBottom:10}}>

                    <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                    {
                      item.reading_status ==  0
                      ? <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                      : <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                    }

                    </View>


            </View>


              </TouchableWithoutFeedback>

        );
      }



      else  if(item.attachment != null ){

        return (

          <View style={{width:'100%',margin:3}}>

          <Card style={{width:null,height:null,padding:5,alignSelf:'flex-end'}}>
          <TouchableWithoutFeedback>
            <View style={{width:'100%'}}>


            <Image style={{height:180,width:180}} source={{uri: urls.base_url +item.attachment}} />

            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10,marginBottom:10,marginTop:10}}>

                    <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                    {
                      item.reading_status ==  0
                      ? <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                      : <Image
                      style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                    }

                    </View>


            </View>


              </TouchableWithoutFeedback>
          </Card>

          </View>
        );

      }
   }




   else{
    if(item.message != null ){
      return (


        <TouchableOpacity onLongPress={()=> this.copyText(item.message)}>
        <View style={{width:Dimensions.get('window').width * 0.7,margin:3,alignSelf:'flex-start'
        ,flexDirection:'row',alignItems:'center'}}>
        <Image source={{uri:urls.base_url+this.state.user_image}}
                        style={{height:20,width:20,overflow:'hidden',
                        borderRadius:20,marginLeft:5,marginRight:10}} />

                <View style={{ backgroundColor:colors.color_primary,
                width:null,height:null,alignSelf:'flex-start',flexDirection:'row',alignItems:'center'
                ,borderRadius:5}}>

                  <Text style={{fontSize:15,marginRight:4,color:'black'
                 ,padding:10,width:'auto',
                  alignSelf:'flex-start',borderRadius:5,alignSelf:'flex-start'}}>{item.message}</Text>

                  <View style={{flexDirection:'row',
                  justifyContent:'space-between',alignItems:'center',marginLeft:10,
                  marginRight:10}}>

                  <Text style={{marginRight:10,fontSize:12}}>{item.created_at.toString().substring(11,16)}</Text>



                  </View>

                  </View>

        </View>
        </TouchableOpacity>
      );
    }


    else if(item.address != null){
      return (


        <TouchableWithoutFeedback 
        onPress={()=> this.seeLocation(item.address,item.lat,item.lng)}>

        <View style={{width:null,padding:0,alignSelf:'flex-start',
     flexDirection:'row',alignItems:'center'}}>

        <Image source={{uri:urls.base_url+this.state.user_image}}
                        style={{height:20,width:20,overflow:'hidden',
                        borderRadius:20,marginLeft:5,marginRight:10}} />

        <View style={{width:null,padding:10,alignSelf:'flex-start',
      margin:0 ,flexDirection:'row',alignItems:'center',backgroundColor:'white'}}>


        <Image style={{width:60, height: 60,margin:5}}  source={require('../assets/location-view.png')} />

        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10,marginBottom:10,marginTop:10,alignSelf:'flex-end'}}>

                    <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                    </View>


                    </View>





        </View>


          </TouchableWithoutFeedback>
      );
    }

    else  if(item.flinks != null ){

      return (

        <View style={{width:'100%',margin:3,height:null,padding:0}}>

        <View style={{width:'70%',height:null,padding:5,alignSelf:'flex-start'
         ,flexDirection:'row',alignItems:'center'}}>

        <Image source={{uri:urls.base_url+this.state.user_image}}
                        style={{height:20,width:20,overflow:'hidden',
                        borderRadius:20,marginLeft:5,marginRight:10}} />

        <TouchableWithoutFeedback>
          <View style={{width:'100%',justifyContent:'space-between'}}>
          <Image style={{height:120,width:120}} source={{uri: urls.base_url+item.flinks}} />
          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10,marginBottom:10,marginTop:10,alignSelf:'flex-start'}}>

                    <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>


                    </View>




          </View>


            </TouchableWithoutFeedback>
            </View>

        </View>
      );

    }



    else  if(item.attachment != null ){

      return (

        <View style={{width:'70%',margin:0 ,
        flexDirection:'row',alignItems:'center'}}>

          <Image source={{uri:urls.base_url+this.state.user_image}}
                        style={{height:20,width:20,overflow:'hidden',
                        borderRadius:20,marginLeft:5,marginRight:10}} />

        <Card style={{width:null,height:null,padding:5,alignSelf:'flex-start'
        }}>
       
        <TouchableWithoutFeedback>
          <View style={{width:'100%'}}>


          <Image style={{height:180,width:180}} source={{uri: urls.base_url +item.attachment}} />

          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                    marginRight:10,marginBottom:10,marginTop:10}}>

                    <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>


                    </View>



          </View>


            </TouchableWithoutFeedback>
        </Card>

        </View>
      );

    }
   }
  }

  onSelect = data => {
    //this function is used heere as a callback ...
    //using this function we get the data from filters page
    //ToastAndroid.show("Recieved data is "+ JSON.stringify(data) ,ToastAndroid.LONG);

    let address = data.address
    let lat = data.latitude
    let lng = data.longtitude


    if(this.state.block_status == false){
      this.sendLocation(address,lat,lng)
    }
    else{
      Platform.OS === 'android'
      ?   ToastAndroid.show(this.state.whoblock, ToastAndroid.SHORT)
      : Alert.alert(this.state.whoblock)
    }


    //this.setState(data);
  };


  onContentOffsetChanged = (distanceFromTop: number) => {
    distanceFromTop === 0 && console.log("TOP")
    
}


getMargin(){

            
  if(Platform.OS == 'android'){
    if(this.state.other_type){
      return 0
    }
    else{
      return 70 + this.state.bottom
    }
  }
  else{
    return 70 + this.state.bottom
  }
}

    render() {




        return (

         <View
         style={styles.container}>






         {/* headrer */}
         <Header

              barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

             leftComponent={
               <TouchableWithoutFeedback onPress={() =>{
                this.setState({type_status:false})
                 this.props.navigation.goBack()}
                 }>
                          <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/back2.png')} />
                       </TouchableWithoutFeedback>
             }


              rightComponent={

                    <View style={{height:'auto',width:'auto',flexDirection:'row',alignItems:'center'}}>




                  <TouchableWithoutFeedback onPress={() => {
                     this.setState({type_status:false})
                    this.next()}}>
                       <View  style={{height:35,width:35,borderRadius:20,borderColor:'white',borderWidth:2}}>
                       <Image source={{uri:urls.base_url+this.state.user_image}}
                        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:20}} />
                        </View>
                       </TouchableWithoutFeedback>

                    <TouchableOpacity onPress={() => {
                      this.setState({type_status:false})
                      this.props.navigation.navigate("SendLocation", { onSelect: this.onSelect })}
                      }>

                    <Image style={{width: 35, height: 35,margin:5}}  source={require('../assets/getlocation.png')} />


                    </TouchableOpacity>


                       </View>

               }


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

             centerComponent={{ text: this.state.user_name, style: { fontSize: 19, color: "white"} }}
              outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
              containerStyle={{

                backgroundColor: '#262626',
                justifyContent: 'space-around',
                alignItems:'center',
                shadowOpacity:1.7,
                shadowOffset: {
                   width: 0,
                   height: 3,
               },
               shadowOpacity: 0.3,
               shadowRadius: 2,
                elevation:7
              }}
            />


                     {/* body */}

                     <View style={{width:'100%',paddingBottom:0,backgroundColor:'#F5F0F0',flex:1}}>





                     <FlatList
                           style={{marginBottom:this.getMargin(),
                           flex:1}}
                           data={this.state.chats}
                            numColumns={1}
                            disableVirtualization={false}
                          
                            onScroll={
                                (event: NativeSyntheticEvent<NativeScrollEvent>) => 
                                    this.onContentOffsetChanged(event.nativeEvent.contentOffset.y)
                            }
                            ref={ (ref) => { this.myFlatListRef = ref } }
                           onContentSizeChange={ () => { this.myFlatListRef.scrollToEnd({animated:true}) } }
                          
                            //onLayout={ () => { this.myFlatListRef.scrollToEnd({animated:true}) } }
                            onEndReached={()=> console.log("Bottom")}
                            showsVerticalScrollIndicator={false}
                           renderItem={this.renderItem}

                         />

                                                                    {
                                                                      this.state.other_type
                                                                      ?
                                                                      <Text 
                                                                      style={{marginBottom:70,color:'green'}}>
                                                                      {this.state.user_name} is typing...</Text>
                                                                      : null
                                                                    }



                       </View>








            <View style={{position:'absolute',
            bottom:this.state.bottom,
            left:0,right:0,height:70,backgroundColor:'white',flexDirection:'row',padding:5,alignItems:'center'}}>
                  <View style={{flex:5}}>



                  <TextInput
                      value={this.state.message}
                      style={styles.input}
                      ref={(component) => this._textInput = component}
                      //onFocus={()=> this.setState({type_status:true})}
                      onEndEditing={()=> this.setState({type_status:false})}
                      onSubmitEditing={()=> this.setState({type_status:false})}
                      placeholder={this.state.block_status ? this.state.whoblock : ''}
                      placeholderTextColor="red"
                      editable={!this.state.block_status}
                      onChangeText={ (message) => {
                        var a = message.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '')
                        this.setState({message:a,type_status:true})
                      } }


             />


                  </View>


                  {
                    this.state.show_flings
                    ?
                    <View style={{flex:1}}>
                  <TouchableWithoutFeedback onPress={()=> this.setState({
                    flink_status:true
                  })}>
                  <Image source={require('../assets/fling.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </TouchableWithoutFeedback>
                  </View>
                  :
                  null


                  }


                  <View style={{flex:1}}>
                  <TouchableWithoutFeedback onPress={()=> this.checkBlockStatusPhoto()}>
                  <Image source={require('../assets/cam.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </TouchableWithoutFeedback>
                  </View>


                 {
                   this.state.message.length > 0
                   ?  <View style={{flex:1}}>
                   <TouchableWithoutFeedback onPress={()=> this.checkBlockStatus()}>
                   <Image source={require('../assets/send.png')} style={{alignSelf:'center',height:35,width:35}} />
                   </TouchableWithoutFeedback>
                   </View>
                   :null
                 }
            </View>
            {
              this.state.flink_status
              ?


               <View style={{position:'absolute',bottom:0,left:0,right:0,height:250,backgroundColor:'white'}}>

               <ScrollView style={{width:'100%',padding:10,flex:1}}>

               <View style={{flex:1,alignSelf:'flex-end'}}>
               <TouchableWithoutFeedback onPress={()=> this.setState({
                 flink_status:false
               })}>
               <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:30,width:30}} />
               </TouchableWithoutFeedback>
               </View>


               <FlatList
               style={{marginBottom:10}}
               data={this.state.flinks}
                numColumns={3}
              
               showsVerticalScrollIndicator={false}
               scrollEnabled={false}
               renderItem={({item}) =>


               <View style={{width:'32%',margin:3}}>


                  <Card style={{width:null,height:null,padding:5}}>

               {
                item.status == 1 //means not buyied
                ?
              <View style={{position:'absolute',top:0,bottom:0,right:0,left:0,backgroundColor:'grey'}}>

              </View>

              :

              null
              }


                   <TouchableWithoutFeedback onPress={()=> this.sendFlings(item.image,item.status,item.id)}>
                 {/* <TouchableWithoutFeedback onPress={()=> this.setState({redeem_modal:true})}> */}
                  <Image style={{height:90,width:90}} source={{uri: urls.base_url+item.image}} resizeMode="contain"/>
                  </TouchableWithoutFeedback>
                  </Card>



                  </View>

              }
             />



             </ScrollView>

               </View>

               :
               null

            }


            {this.state.loading_status &&
              <View style={styles.loading}>
                <ActivityIndicator size='large' />
              </View>
          }

         </View>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',

    flex:1,



  },
 logo_image:{
  width:'100%',
  //height:Dimensions.get('window').height * 0.2
  height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,

 } ,input: {
  width: "100%",
  height: 45,
 padding:5,
  borderRadius: 19,
  borderWidth: 1,
  borderColor: 'black',
  backgroundColor:'#E4E4EE'

},
loginButton:{

    margin:6,
    width:"100%",
    height:40,
    backgroundColor:'#FFC300',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',


 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',



 },
 social_image:{
  height:50,
  width:'100%'


},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}


}
)

export default withNavigationFocus(ChatBox);




                                           //
                                           //                         {
                                           //   this.state.other_type
                                           //   ?
                                           //   <Text style={{marginBottom:70}}>{this.state.user_name} is typing...</Text>
                                           //   : null
                                           // }
