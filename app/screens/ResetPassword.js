import React, {Component} from 'react';
import {Text, View, Platform,Alert,Image,Dimensions,ToastAndroid,StatusBar,TouchableWithoutFeedback,StyleSheet,ImageBackground,TouchableOpacity,TextInput,ActivityIndicator} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions } from 'react-navigation';
import I18n from '../i18n';
import {Header} from 'react-native-elements';
import { colors,fonts,urls } from './Variables';
import { ScrollView } from 'react-native-gesture-handler';

export default class ResetPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          old_password:'',
          new_password:'',
          confirm_password:'',

      };

    }

    isValid() {
     

      let valid = false;

      if (this.state.old_password.trim().length > 0  && this.state.new_password.trim().length > 0 && this.state.confirm_password.trim().length > 0) {
        valid = true;
      }

      if (this.state.old_password.trim().length === 0) {
        
      

        Platform.OS === 'android' 
        ?   ToastAndroid.show("Enter old password", ToastAndroid.SHORT)
        : Alert.alert("Enter old password")


        return false;
      }
      else if(this.state.new_password.trim().length === 0){

      


        Platform.OS === 'android' 
        ?   ToastAndroid.show("Enter new password", ToastAndroid.SHORT)
        : Alert.alert("Enter new password")


        return false;
      }
      else if (this.state.new_password.trim().length < 8 || this.state.new_password.trim().length > 16) {
       
      

        Platform.OS === 'android' 
        ?   ToastAndroid.show("Password should be 8-16 characters long", ToastAndroid.SHORT)
        : Alert.alert("Password should be 8-16 characters long")


        return false;
      }

      else if(this.state.confirm_password.trim().length === 0){

       // ToastAndroid.show('Enter confirm password', ToastAndroid.SHORT);


        Platform.OS === 'android' 
        ?   ToastAndroid.show("Enter confirm password", ToastAndroid.SHORT)
        : Alert.alert("Enter confirm password")


        return false;
      }
      else if (this.state.new_password.trim().toString() != this.state.confirm_password.trim().toString()) {
       
        //ToastAndroid.show('Password and confirm password should match', ToastAndroid.SHORT);


        Platform.OS === 'android' 
        ?   ToastAndroid.show("Password and confirm password should match", ToastAndroid.SHORT)
        : Alert.alert("Password and confirm password should match")


        return false;
      }
     

      return valid;
  }


    onReset(){

           
            if(this.isValid()){


              AsyncStorage.getItem("user_id").then((item) => {
                if (item) {
                  var formData = new FormData();


                  formData.append('id',item);
                  formData.append('new_password', this.state.new_password);
                  formData.append('old_password', this.state.old_password);
                  formData.append('confirm_password', this.state.confirm_password);
    
                                this.setState({loading_status:true})
                                let url = urls.base_url +'api/api_reset_password'
                                fetch(url, {
                                method: 'POST',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'multipart/form-data',
                                },
                            body: formData
    
                              }).then((response) => response.json())
                                    .then((responseJson) => {
                            this.setState({loading_status:false})
                          // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                        if(!responseJson.error){
                                        
                                        ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                        AsyncStorage.clear();
                                        const popAction = StackActions.pop({  n: 1,  });

                                        this.props.navigation.dispatch(popAction);
                                        this.props.navigation.navigate('Login');
                                      
                                         
                                         // this.props.navigation.navigate("Welcome");
                                        //this.props.navigation.navigate("HomeScreen");
                                      }
                                      else{
    
                                       
                                          Platform.OS === 'android' 
                                          ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                          : Alert.alert(responseJson.message)
                                      }
                                    }).catch((error) => {
                                      this.setState({loading_status:false})
                                      
                                        Platform.OS === 'android' 
                                        ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                        : Alert.alert("Connection Error !")
                                    });
                }
                else{
                  ToastAndroid.show("User not found !",ToastAndroid.LONG)
                }
              });

              


            }

}

remove = async () =>{

  await AsyncStorage.removeItem('uname', (err) => {
    const popAction = StackActions.pop({  n: 1,  });

  this.props.navigation.dispatch(popAction);
  this.props.navigation.navigate('Login');

  });

}


componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });



  this.setState({
    old_password:''
  })
}


    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
      
    }

    render() {


        return (
      
         <View 
         style={styles.container}>


      <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.navigate("HomeScreen")
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                  

                  
          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
           
            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
          <View style={{justifyContent:'center',alignItems:'center',padding:20,flex:1}}>
         
          <Text style={{alignSelf:'center',color:'black',fontSize:19,marginBottom:10}}>{I18n.t('reset_password')}</Text>
          <Text  style={{alignSelf:'center',marginBottom:30}}>{I18n.t('please_update_your_password')}</Text>


          <Text style={styles.input_text}>{I18n.t('old_password')}</Text>
          <TextInput
            value={this.state.old_password}
            secureTextEntry={true}
            onChangeText={(old_password) => this.setState({ old_password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />

         <Text style={styles.input_text}>{I18n.t('new_password')}</Text>
          <TextInput
            value={this.state.new_password}
            secureTextEntry={true}
            onChangeText={(new_password) => this.setState({ new_password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />


          <Text style={styles.input_text}>{I18n.t('confirm_password')}</Text>
          <TextInput
            value={this.state.confirm_password}
            secureTextEntry={true}
            onChangeText={(confirm_password) => this.setState({ confirm_password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />  

          <TouchableOpacity
          onPress={this.onReset.bind(this)}
                style={styles.resetButton}>
                <Text style={styles.resetText}>{I18n.t('reset').toUpperCase()}</Text>
          </TouchableOpacity>

          {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }
     
        </View>

         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    
    flex:1,
   
  
    
  } ,
 input_text:{
  width:'100%',
  marginBottom:10
 },

 input: {
  width: "100%",
  height: 50,
 padding:4,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
  resetButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  resetText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  logo_image:{
    width:'100%',
    //height:Dimensions.get('window').height * 0.2
    height:'30%'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }
 
 
  

}
)
