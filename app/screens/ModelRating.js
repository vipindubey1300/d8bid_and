import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,Platform,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
//import {  } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { Chip } from 'react-native-paper';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import RBSheet from "react-native-raw-bottom-sheet";
import DismissKeyboard from 'dismissKeyboard';
import AsyncStorage from '@react-native-community/async-storage';

import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import { Rating, AirbnbRating } from 'react-native-ratings';
import I18n from '../i18n';
import { Slider,Overlay ,Header} from 'react-native-elements';

export default class ModelRating extends React.Component {
    constructor(props) {
        super(props);
        this.ratingCompleted = this.ratingCompleted.bind(this)
        this.state ={
          chips:[],
          gallery:[],
          name:null,
          age:null,
          height:null,
          weight:null,
          user_image:null,
          about:null,
          state:null,
          live:null,
          user_image:null,
          loading_status:false,
          rating:1.5,
          ethnicities:null,
          body_type:null,
          block_status:false


      };


    }





    getUserId= async () =>{
      var id = await AsyncStorage.getItem('user_id')
      return id
    }











//fetch = async () =>
    fetch(model_id){
      this.setState({loading_status:true})

      var formData = new FormData();


      formData.append('model_id', model_id);
                   let url = urls.base_url +'api/models_detail'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type':  'multipart/form-data',
                      },
                      body: formData

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             // var id = responseJson.result.model_detail.id
                             var name = responseJson.result.model_detail.name
                             var live = responseJson.result.model_detail.live
                             var age = responseJson.result.model_detail.age
                             var user_image = responseJson.result.model_detail.user_image
                             var about = responseJson.result.model_detail.about
                             var height = responseJson.result.model_detail.height
                             var weight = responseJson.result.model_detail.weight
                             var state = responseJson.result.model_detail.state


                             var ethnicities = responseJson.result.ethnicities[0].name
                             var body_type = responseJson.result.body_type[0].name



                             var rev = responseJson.result.reviews
                             var stars = responseJson.result.star
                             var reviews = rev.toString().length == 0 ? 0: parseFloat(rev)

                             reviews =  parseFloat(reviews) + parseFloat(stars)

                             var gallery = responseJson.gallery
                             var len = gallery.length
                             var temp_arr=[]


                            // ToastAndroid.show(reviews.toString(), ToastAndroid.LONG);
                             for(var i = 0 ; i < 5 ; i++){
                              var id = responseJson.gallery[0].id
                              var profile_pic = responseJson.gallery[0].profile_pic

                              var image_one = responseJson.gallery[0].image_one
                              var image_two = responseJson.gallery[0].image_two
                              var image_three = responseJson.gallery[0].image_three
                              var image_four = responseJson.gallery[0].image_four

                              if(i == 0 && profile_pic != null ){
                                var image = responseJson.gallery[0].profile_pic
                                const array = [...temp_arr];
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], image: image};
                                temp_arr = array
                              }
                              else if(i == 1 && image_one != null){
                                var image = responseJson.gallery[0].image_one
                                const array = [...temp_arr];
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], image: image};
                                temp_arr = array
                              }
                              else if(i == 2 && image_two != null){
                                var image = responseJson.gallery[0].image_two
                                const array = [...temp_arr];
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], image: image};
                                temp_arr = array
                              }
                              else if(i == 3 && image_three != null){
                                var image = responseJson.gallery[0].image_three
                                const array = [...temp_arr];
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], image: image};
                                temp_arr = array
                              }
                              else if(i == 4 && image_four != null){
                                var image = responseJson.gallery[0].image_four
                                const array = [...temp_arr];
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], image: image};
                                temp_arr = array
                              }



                              }

                               this.setState({gallery : temp_arr});


                             this.setState({
                              name:name,
                              about:about,
                              height:height,
                              weight:weight,
                              age:age,
                              user_image:user_image,
                              live:live,
                              state:state,
                              rating:reviews,
                              ethnicities:ethnicities,
                              body_type:body_type


                            })

                              var interest = responseJson.result.interest_tag
                              var length = interest.length

                              for(var i = 0 ; i < length ; i++){

                                var name_temp = interest[i].name   //interest name

                                      // Create a new array based on current state:
                                      let interests = [...this.state.chips];
                                     // Add item to it
                                      interests.push( name_temp );



                                      this.setState({ chips:interests});




                                }


                            }


                          else{
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Profile not found !", ToastAndroid.SHORT)
                            : Alert.alert("Profile not found !")
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android'
                ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                : Alert.alert("Connection Error !")
                          });



    }

    _renderDotIndicator() {
      return <PagerDotIndicator pageCount={this.state.gallery.length} />;
    }

    onRating(){

        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
            var formData = new FormData();

            var result  = this.props.navigation.getParam('result')
           var model_id = result['model_id']


            formData.append('user_id',item);
            formData.append('model_id', model_id);
            formData.append('rating', this.state.rating);
           // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);

                          this.setState({loading_status:true})
                          let url = urls.base_url +'api/send_review'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                                 this.setState({loading_status:false})
                    // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                  if(!responseJson.error){

                                    Platform.OS === 'android'
                                    ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                    : Alert.alert(responseJson.message)


                                }
                                else{


                                    Platform.OS === 'android'
                                    ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                    : Alert.alert(responseJson.message)
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                  Platform.OS === 'android'
                                  ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                  : Alert.alert("Connection Error !")
                              });
          }
          else{
           // ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });






}

checkBlockStatus(){


  AsyncStorage.getItem("user_id").then((item) => {
    if (item) {

      var result  = this.props.navigation.getParam('result')
  
     
  var formData = new FormData();


  formData.append('form_id', item);
  formData.append('to_id', result['model_id']);
  //formData.append('message', this.state.message);





                  this.setState({loading_status:true})
                  let url = urls.base_url +'api/block_status'

                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                }).then((response) => response.json())
                      .then((responseJson) => {
                 this.setState({loading_status:false})


                 console.log("RRRRRRR",responseJson)
            // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                           if(!responseJson.error){

                        this.setState({block_status:true})
                          
                        }
                        else{

                          // Platform.OS === 'android' 
                          // ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          // : Alert.alert(responseJson.message)

                          //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})
                        
                        Platform.OS === 'android' 
                        ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error !")
                      });
    }
    else{
      ToastAndroid.show("User not found !",ToastAndroid.LONG)
    }
  });





}

onBlock(){


  AsyncStorage.getItem("user_id").then((item) => {
    if (item) {

      var result  = this.props.navigation.getParam('result')
  
     
  var formData = new FormData();


  formData.append('form_id', item);
  formData.append('to_id', result['model_id']);
  //formData.append('message', this.state.message);





                  this.setState({loading_status:true})
                  let url = urls.base_url +'api/block_chat'

                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                }).then((response) => response.json())
                      .then((responseJson) => {
                 this.setState({loading_status:false})
            // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                           if(!responseJson.error){

                        
                            Platform.OS === 'android' 
                          ?  ToastAndroid.show('Blocked successfully ', ToastAndroid.SHORT)
                          : Alert.alert('Blocked successfully')

                          this.props.navigation.navigate("ChatList")
                         
                          
                        }
                        else{

                          Platform.OS === 'android' 
                          ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          : Alert.alert(responseJson.message)

                          //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})
                        
                        Platform.OS === 'android' 
                        ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error !")
                      });
    }
    else{
      ToastAndroid.show("User not found !",ToastAndroid.LONG)
    }
  });





}

onUnblock(){


  AsyncStorage.getItem("user_id").then((item) => {
    if (item) {

      var result  = this.props.navigation.getParam('result')
  
     
  var formData = new FormData();


  formData.append('form_id', item);
  formData.append('to_id', result['model_id']);
  //formData.append('message', this.state.message);





                  this.setState({loading_status:true})
                  let url = urls.base_url +'api/unblock_chat'

                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                }).then((response) => response.json())
                      .then((responseJson) => {
                 this.setState({loading_status:false})
            // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                           if(!responseJson.error){

                        
                            Platform.OS === 'android' 
                          ?  ToastAndroid.show('Unblocked successfully ', ToastAndroid.SHORT)
                          : Alert.alert('Unblocked successfully')

                          this.props.navigation.navigate("ChatList")
                         
                          
                        }
                        else{

                          Platform.OS === 'android' 
                          ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          : Alert.alert(responseJson.message)

                          //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})
                        
                        Platform.OS === 'android' 
                        ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error !")
                      });
    }
    else{
      ToastAndroid.show("User not found !",ToastAndroid.LONG)
    }
  });





}




componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

  this.checkBlockStatus()


}






    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     var result  = this.props.navigation.getParam('result')
     var model_id = result['model_id']
     this.fetch(model_id)

    }

    ratingCompleted(rating) {


          this.setState({
            rating:rating
          })
    }

    alert(){

      Alert.alert(
        'Block',
        'Block this user from seeing you. Note: this will only block the user from seeing you alone.  If you would like to make a formal complaint to D8bid about another users behaviour, please go to “contact us” on your side bar and follow prompts for us to take all necessary action regarding your complaint. D8bid will review all complaints within 24hours',
        [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
        {
          text: 'OK',
          onPress: () => this.onBlock()
        }
        ],
        {
        cancelable: false
        }
      );
    }


    render() {


      let makeGallery =this.state.gallery.map((gallery) => {
        return (


          <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>

          <Image source={{uri:urls.base_url +gallery.image}} style={{alignSelf:'center',height:'100%',width:'100%'}} />

        </View>
        );
    })





      let chips = this.state.chips.map((r, i) => {

        return (
              <Chip
              style={{width:null,margin:4,backgroundColor:colors.color_primary}}
              mode={'outlined'}
              selected={false}
              selectedColor={'black'}
              onPress={() => {

                 {/*      let ids = [...this.state.interest_boolean];
                      ids[i] =!ids[i];
                      this.setState({interest_boolean: ids });
                      */}
              }}>
              {r}
              </Chip>
        );
})


        return (

         <View
         style={styles.container}>

         {/*
         <Header
         statusBarProps={{ barStyle: 'light-content' }}
         barStyle="light-content" // or directly
         leftComponent={{ icon: 'menu', onPress: () => console.log('pressed') }}
         centerComponent={{ text: 'MY TITLE' }}
         rightComponent={{ icon: 'home' }}
         containerStyle={{
          backgroundColor:colors.color_primary,
          justifyContent: 'space-around',
          alignItems:'center'
        }}
       />

       */}

         {/*for header*/}
         <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                
          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
           
        centerComponent={{ text: I18n.t('model_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />


       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.4,width:'100%'}}>


           <IndicatorViewPager
           style={{ height: '100%' }}
           indicator={this._renderDotIndicator()}>
          {makeGallery}
         </IndicatorViewPager>


           </View>


        {/*middle f;oating element */}
         {/*

              <View style={{flexDirection:'row',justifyContent:'space-around',width:'40%',alignSelf:'flex-end',marginLeft:'50%',marginTop:-25}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("ModelBidding")}>
                              <Image style={{width: 55, height: 55,margin:1}}  source={require('../assets/accept.png')} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("ModelProtest")}>
                      <Image style={{width: 55, height: 55,margin:1}}  source={require('../assets/reject.png')} />
                      </TouchableOpacity>


              </View>
              */}

          {/*end */}

           <View style={{flex:0.5,padding:20,marginTop:15,width:'100%'}}>


<View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                  <View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <Text style={{fontSize:18,marginRight:7,color:'black',fontWeight:'bold'}}>{this.state.name}</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:7}}>,</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:17,color:'black',marginTop:1}}>{this.state.age}</Text>

                                        <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />


                    </View>
                  <Text style={{alignSelf:'flex-start',fontSize:13}}>{this.state.state}</Text>
                  </View>

<View>
        {/*
 <Rating
           type='heart'
           type='custom'

               ratingColor={colors.color_primary}
               ratingBackgroundColor={colors.color_secondary}
         ratingCount={5}
         minValue={this.state.rating}
         imageSize={25}
           onFinishRating={this.ratingCompleted}
     />
        */}


        <AirbnbRating
        count={5}
        onFinishRating={this.ratingCompleted}
        defaultRating={this.state.rating}
        size={20}
  />


     <Text onPress={() => this.onRating()} style={{alignSelf:'center',fontSize:15,color:'black',marginTop:5,fontWeight:'bold'}}>{I18n.t('rate')}</Text>
     </View>



     </View>



                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        : <Text style={{color:'grey'}}>{this.state.about}</Text>
                      }

                      </View>


                  </View>





                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('height').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'grey'}}>{this.state.height} {I18n.t('cms')}</Text>
                      </View>


                  </View>

                   {/*Weight */}

                   <View style={{marginTop:35,marginLeft:20}}>
                   <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                       <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                       <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('weight').toUpperCase()}</Text>

                   </View>
                   <View style={{marginLeft:30}}>
                   <Text style={{color:'grey'}}>{this.state.weight} {I18n.t('kgs')}</Text>
                   </View>


               </View>

               {/*ethniicty */}

               <View style={{marginTop:35,marginLeft:20}}>
                    <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                        <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                        <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('ethnicity').toUpperCase()}</Text>

                    </View>



                      <View style={{marginLeft:30}}>
                      {
                        this.state.ethnicities == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.ethnicities} </Text>
                      }

                      </View>

           </View>


            {/*bodytype */}


           <View style={{marginTop:35,marginLeft:20}}>
           <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
               <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
               <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('body_type').toUpperCase()}</Text>

           </View>



           <View style={{marginLeft:30}}>
           {
             this.state.body_type == null
             ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
             :  <Text style={{color:'grey'}}>{this.state.body_type} </Text>
           }

           </View>

  

       {
         this.state.block_status == false
         ?
         <View style={{alignSelf:'flex-end',
           backgroundColor:colors.color_primary,
           borderColor:'black',borderRadius:8,borderWidth:2,
           height:40,width:null,justifyContent:'center',alignItems:'center',marginTop:20}}>

           <TouchableOpacity onPress={()=> {this.alert()
       
           }}>
           <Text style={{margin:10}}>{I18n.t('block')}</Text>
           </TouchableOpacity>

           </View>
           :
           <View style={{alignSelf:'flex-end',
           backgroundColor:colors.color_primary,
           borderColor:'black',borderRadius:8,borderWidth:2,
           height:40,width:null,justifyContent:'center',alignItems:'center',marginTop:20}}>

           <TouchableOpacity onPress={()=> {this.onUnblock()
       
           }}>
           <Text style={{margin:10}}>{I18n.t('unblock')}</Text>
           </TouchableOpacity>

           </View>
       }

  

       </View>



                  {/*interest */}
{/*
                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/grey.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>INTEREST</Text>

                      </View>



                      <View style={{flexDirection:'row',flexWrap: 'wrap',marginLeft:30}}>
                      {chips}
                      </View>


                  </View>

                  */}

           </View>



         </View>

         <RBSheet
         ref={ref => {
             this.RBSheet = ref;
         }}
         height={400}
         duration={250}
         customStyles={{
             container: {
             justifyContent: "center",
             alignItems: "center"
             }
         }}
         >

         <View style={{height:'100%',justifyContent:'center',alignItems:'center',width:'100%'}}>
         <View style={{height:'75%'}}>

         <Text style={{fontSize:18,margin:10,color:'black',marginTop:-5,marginBottom:20}}>{I18n.t('choose_bid_amout_dollars')}</Text>
         <ScrollPicker
              dataSource={[
                '10',
                '20',
                '30',
                '40',
                '50',
                '60',
                '70',
                '80',
                '90',
                '100',
                '110',
                '120',
                '130',
                '140',
                '150',
                '160',
                '170',
                '180',
                '190',
                '200',
                '210',
                '220',
                '230',
                '240',
                '250',
                '260',
                '270',
                '280',
                '290',
                '300',
                '325',
                '350',
                '375',
                '400',
                '425',
                '450',
                '475',
                '500',
                '525',
                '550',
                '575',
                '600',
                '625',
                '650',
                '675',
                '700',
                '725',
                '750',
                '775',
                '800',
                '825',
                '850',
                '875',
                '900',
                '925',
                '950',
                '975',
                '1000',
                '1025',
                '1050',
                '1075',
                '1100',
                '1125',
                '1150',
                '1175',
                '1200',
                '1225',
                '1250',
                '1275',
                '1300',
                '1325',
                '1350',
                '1375',
                '1400',
                '1425',
                '1450',
                '1475',
                '1500',
                '1525',
                '1550',
                '1575',
                '1600',
                '1625',
                '1650',
                '1675',
                '1700',
                '1725',
                '1750',
                '1775',
                '1800',
                '1825',
                '1850',
                '1875',
                '1900',
                '1925',
                '1950',
                '1975',
                '2000',


              ]}
              selectedIndex={10}
              renderItem={(data, index, isSelected) => {

                  //
              }}
              onValueChange={(data, selectedIndex) => {
                  //
                  this.setState({
                      index:selectedIndex,
                      bid_data:data
                  })
                  //ToastAndroid.show(data,ToastAndroid.SHORT)

              }}
              wrapperHeight={70}
              wrapperWidth={150}
              wrapperBackground={'#FFFFFF'}
              itemHeight={40}
              highlightColor={'#d8d8d8'}
              highlightBorderWidth={4}
              activeItemColor={'#222121'}
              itemColor={'#B4B4B4'}
            />
            </View>



            <TouchableOpacity
            style={styles.submitButton}
            onPress={()=>{
              this.RBSheet.close()
            }}


            >
            <Text style={styles.submitText}>{I18n.t('select').toUpperCase()}</Text>
            </TouchableOpacity>




             </View>
         </RBSheet>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },

    submitButton:{

      marginBottom:5,
      marginTop:10,
      width:"90%",
      height:50,
      backgroundColor:colors.color_primary,
      borderRadius:10,
      borderWidth: 1,
      borderColor: 'black',


   },
   submitText:{
     color:'black',
     textAlign:'center',
     fontSize :18,
     padding:10

   },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },

 bid_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',

},


bidButton:{


  width:"100%",
  height:50,
  alignSelf:'center',
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:colors.color_secondary,
  borderRadius:8,
  borderWidth: 1,
  borderColor: 'black',
  marginLeft:40


},
bidText:{
 color:'white',
 textAlign:'center',
 fontSize :18,
 fontWeight : 'bold'
}
,
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}




}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
