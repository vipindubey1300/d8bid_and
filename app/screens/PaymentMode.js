import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator,Platform,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,urls } from './Variables';
import {Card} from "native-base";
import PayPal from 'react-native-paypal-wrapper';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import stripe from 'tipsi-stripe'
import {Header} from 'react-native-elements';

stripe.setOptions({
  publishableKey: 'pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2',
 //sandox:pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB
 //live : pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2

 //androidPayMode: 'test',
androidPayMode:'production'
})

export default class PaymentMode extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          price:'',
          loading_status:false

      };


    }

    async stripe(){
      const options = {
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          email:'abc@gmail.com',
          billingAddress: {
            name: 'Gunilla Haugeh',
            line1: 'Canary Place',
            line2: '3',
            city: 'Macon',
            state: 'Georgia',
            country: 'US',
            postalCode: '31217',
          },
        },
      }

      const token = await stripe.paymentRequestWithCardForm(options)
    // ToastAndroid.show(JSON.stringify(token.tokenId),ToastAndroid.LONG)
      var cardId = token.card.cardId
      var tokenId = token.tokenId
    this.stripeApi(tokenId)
      console.log("carddd",JSON.stringify(token))
    }

    convertDate(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()+1).toString();
      var dd  = date.getDate().toString();

      var mmChars = mm.split('');
      var ddChars = dd.split('');

      return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
    }


    getUserId= async () =>{
      var id = await AsyncStorage.getItem('user_id')
      return id
    }

    stripeApi(token_id){


      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {

          var result  = this.props.navigation.getParam('result')
          var role_id = result["role_id"]
          var price = result["price"]



          var formData = new FormData();
          //txnid and payal id should have same value
          formData.append('user_id',item);
          formData.append('stripe_token',token_id);


          if(role_id == 2)[
            //model
            formData.append('star_id',result["star_id"])
          ]
          else if(role_id == 3){
            formData.append('coin_id',result["coin_id"])
          }
          console.log(JSON.stringify(formData))

          //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
            this.setState({loading_status:true})


            let url = urls.base_url +'api/strip_payment'
          fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type':  'multipart/form-data',
          },
          body: formData

        }).then((response) => response.json())
              .then((responseJson) => {
                this.setState({loading_status:false})
             //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
              if(!responseJson.error){
                    //success in inserting data


                    Platform.OS === 'android'
                    ?  ToastAndroid.show("Payment Successful ", ToastAndroid.SHORT)
                    : Alert.alert("Payment Successful ")


                    if(role_id == 2){
                      //model

                     // ToastAndroid.show("modellel", ToastAndroid.LONG);

                      var result  = this.props.navigation.getParam('result')
                      var star = result["star"]

                      AsyncStorage.getItem( 'stars' )
                            .then( data => {

                              // the string value read from AsyncStorage has been assigned to data
                              console.log( data );
                                var c = parseInt(data)
                                c= c + parseInt(star)

                              //save the value to AsyncStorage again
                              AsyncStorage.setItem( 'stars', c.toString() );

                            }).done();


                      this.props.navigation.navigate('HomeScreen');
                    }



                    else if(role_id == 3){
                      var result  = this.props.navigation.getParam('result')
                      var coin = result["coin"]

                                  AsyncStorage.getItem( 'coins' )
                .then( data => {

                  // the string value read from AsyncStorage has been assigned to data
                  console.log( data );
                    var c = parseInt(data)
                    c= c + parseInt(coin)

                  //save the value to AsyncStorage again
                  AsyncStorage.setItem( 'coins', c.toString() );

                }).done();
                     // AsyncStorage.setItem("coins", coin.toString());
                      this.props.navigation.navigate('HomePage');
                    }


            }else{



                      Platform.OS === 'android'
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)
              }


              }).catch((error) => {
               // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
                Platform.OS === 'android'
                ?  ToastAndroid.show(error.message, ToastAndroid.SHORT)
                : Alert.alert("Connection Error !")

              });
        }
        else{
          //ToastAndroid.show("User not found !",ToastAndroid.LONG)
          // Platform.OS === 'android'
          // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
          // : Alert.alert("User not found !")
        }
      });

      //  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

      }


    paypalpaymentapi(pid){


      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {

          var result  = this.props.navigation.getParam('result')
          var role_id = result["role_id"]
          var price = result["price"]

          var date = this.convertDate(new Date())

          //ToastAndroid.show("price"+price+"bokingid...."+bid+"uiddd..."+uid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
          var formData = new FormData();
          //txnid and payal id should have same value
          formData.append('user_id',item);
          formData.append('txn_status',1);
          formData.append('txn_id', pid.toString());
          formData.append('txn_amt',price);
          formData.append('txn_date',date.toString());
          formData.append('paypal_id',pid.toString());
          formData.append('role_id',role_id.toString());
          if(role_id == 2)[
            //model
            formData.append('stars',result["star"])
          ]
          else if(role_id == 3){
            formData.append('coins',result["coin"])
          }


          //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
            this.setState({loading_status:true})


            let url = urls.base_url +'api/transaction_paypal'
          fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type':  'multipart/form-data',
          },
          body: formData

        }).then((response) => response.json())
              .then((responseJson) => {
                this.setState({loading_status:false})
               //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
              if(!responseJson.error){
                    //success in inserting data


                    Platform.OS === 'android'
                    ?  ToastAndroid.show("Payment Successful ", ToastAndroid.SHORT)
                    : Alert.alert("Payment Successful ")


                    if(role_id == 2){
                      //model

                     // ToastAndroid.show("modellel", ToastAndroid.LONG);

                      var result  = this.props.navigation.getParam('result')
                      var star = result["star"]

                      AsyncStorage.getItem( 'stars' )
                            .then( data => {

                              // the string value read from AsyncStorage has been assigned to data
                              console.log( data );
                                var c = parseInt(data)
                                c= c + parseInt(star)

                              //save the value to AsyncStorage again
                              AsyncStorage.setItem( 'stars', c.toString() );

                            }).done();


                      this.props.navigation.navigate('HomeScreen');
                    }



                    else if(role_id == 3){
                      var result  = this.props.navigation.getParam('result')
                      var coin = result["coin"]

                                  AsyncStorage.getItem( 'coins' )
                .then( data => {

                  // the string value read from AsyncStorage has been assigned to data
                  console.log( data );
                    var c = parseInt(data)
                    c= c + parseInt(coin)

                  //save the value to AsyncStorage again
                  AsyncStorage.setItem( 'coins', c.toString() );

                }).done();
                     // AsyncStorage.setItem("coins", coin.toString());
                      this.props.navigation.navigate('HomePage');
                    }


            }else{



                      Platform.OS === 'android'
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)
              }


              }).catch((error) => {
               // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
                Platform.OS === 'android'
                ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                : Alert.alert("Connection Error !")

              });
        }
        else{
          //ToastAndroid.show("User not found !",ToastAndroid.LONG)
          // Platform.OS === 'android'
          // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
          // : Alert.alert("User not found !")
        }
      });

      //  ToastAndroid.show(price+"...."+bid+"..."+pid+"...", ToastAndroid.SHORT);

      }


    paypal(){

      var result  = this.props.navigation.getParam('result')
      var price = result["price"]

     //ToastAndroid.show("Initializing paypal..!!!..."+price,ToastAndroid.LONG)

       //PayPal.initialize(PayPal.SANDBOX,"ARFiHtTEV2P17X4Hdei6M4vqoLx_UVGAvELcTpNprLC4hNklLmNq7cl_z5OqhRGoBzzVRmS9sG-RfhXx");
        PayPal.initialize(PayPal.PRODUCTION,"AdCh6VdZcJ04UccfJHSWO7kraBNf2uukImABk5KC5uR8nFNaxl030c3ES92GVLSvJ1_NruLa_jgNlsxR");
        PayPal.pay({
          price: price.toString(),
          currency: 'USD',
          description: 'Payment for Purchase',
        }).then(confirm => {

          var pid = confirm.response.id
          this.paypalpaymentapi(pid)


          //ToastAndroid.show("price"+price+"uiddd..."+pid, ToastAndroid.SHORT);
        })
          .catch(error =>{
            Platform.OS === 'android'
            ?  ToastAndroid.show("Transaction Cancelled", ToastAndroid.SHORT)
            : Alert.alert("Transaction Cancelled")
            //ToastAndroid.show(JSON.stringify(error), ToastAndroid.SHORT);
            //ToastAndroid.show("Transaction declined by user", ToastAndroid.SHORT);
              //this.setState({quantity:1.0,  cash_status:'cash'})

          })


    }



    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     var result  = this.props.navigation.getParam('result')
     var price = result["price"]
     var role_id = result["role_id"]

    }

    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });


    }


    render() {



        return (

         <View
         style={styles.container}>


         {/*for header*/}
         <Header

   barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

  leftComponent={
    <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
  }


   rightComponent={

    <View></View>

    }


   statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

  centerComponent={{ text: I18n.t('payment_mode'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
   outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
   containerStyle={{

     backgroundColor: colors.color_primary,
     justifyContent: 'space-around',
     alignItems:'center',
     shadowOpacity:1.7,
     shadowOffset: {
        width: 0,
        height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2,
     elevation:7
   }}
 />

       {/*for main content*/}


           <View style={styles.body}>
           <Text style={{fontSize: 17, color: "black",marginBottom:20,marginTop:30}}>{I18n.t('select_payment_mode').toUpperCase()}</Text>
{/*
           <Card style={{width:'100%',padding:15,height:null,marginBottom:10}}>
           <TouchableOpacity>
             <View style={{width:'100%',flexDirection:'row',alignItems:'center'}}>
             <Image style={{width: 25, height: 25,marginRight:30}}  source={require('../assets/card.png')} />
             <Text style={{fontSize: 17, color: "black"}}>{I18n.t('credit_debit')}t</Text>
             </View>
           </TouchableOpacity>

           </Card>
           */}

{/*
           <Card style={{width:'100%',padding:15,height:null,marginBottom:10}}>
           <TouchableOpacity>
             <View style={{width:'100%',flexDirection:'row',alignItems:'center'}}>
             <Image style={{width: 25, height: 25,marginRight:30}}  source={require('../assets/wepay.png')} />
             <Text style={{fontSize: 17, color: "black"}}>We-Pay</Text>
             </View>
           </TouchableOpacity>

           </Card>
*/}


    <Card style={{width:'100%',padding:15,height:null,marginBottom:10}}>
           <TouchableOpacity onPress={this.stripe.bind(this)}>
           <View style={{width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
             <View style={{flexDirection:'row',alignItems:'center'}}>
             <Image style={{width: 25, height: 25,marginRight:30}}  source={require('../assets/card.png')} />
             <Text style={{fontSize: 17, color: "black"}}>{I18n.t('credit_card')}</Text>
             </View>
             <Image style={{width: 55, height: 25,marginRight:0}}  source={require('../assets/abc.png')}
             resizeMode="contain"/>
             </View>
           </TouchableOpacity>

           </Card>





           <Card style={{width:'100%',padding:15,height:null,marginBottom:10}}>
           <TouchableOpacity onPress={this.paypal.bind(this)}>
           <View style={{width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                  <View style={{flexDirection:'row',alignItems:'center'}}>
                  <Image style={{width: 25, height: 25,marginRight:30}}  source={require('../assets/paytm.png')} />
                  <Text style={{fontSize: 17, color: "black"}}>{I18n.t('paypal')}</Text>

                  </View>

                  <Image style={{width: 55, height: 25,marginRight:0}}  source={require('../assets/abc.png')}
             resizeMode="contain"/>

             </View>
           </TouchableOpacity>


           </Card>





           </View>

           {  this.state.loading_status &&
                <View pointerEvents="none" style={styles.loading}>
                  <ActivityIndicator size='large' />
                </View>
        }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: '#D2AC45'

  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:15,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },
   indicator: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     height: 80
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
