export const colors = {
  status_bar_color: "#180F07",
  color_primary:'#D2AC45',
  color_secondary:'#180F07'
  //... more colors here
};

export const fonts = {
  font_size: 16,
};

export const urls = {
  base_url: 'https://d8bid.com/',
};

//import { colors, fonts } from 'theme/variables';
// buttonContainer: {
//   backgroundColor: colors.INIT_COLOR,
//   fontSize: fonts.INIT_FONT_SIZE
// }
