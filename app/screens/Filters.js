import React, {Component} from 'react';
import {Text, View, Image,Dimensions,Switch,ToastAndroid,StatusBar,StyleSheet,Platform,
  ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Chip } from 'react-native-paper';
import { colors,fonts,urls} from './Variables';
import { CheckBox } from 'react-native-elements';
import MultiSelect from 'react-native-multiple-select';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';

import {Header} from 'react-native-elements';

export default class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          weight_values: [40, 150],
          height_values: [100, 220],
          age_values: [18, 90],
          date_status:true,
          dance_status:false,
          male_checked:false,
          female_checked:true,
          transgender_check:false,
          location_status:false,
          selectedItems : [],
          cities:[],
          states:[],
          ethnicity:[],
          body:[],
          countries:[],
          country_id:'key0',
          state_id:'key0',
          stars:'key0',
          ethnicity_boolean: [],
          ethnicity_id:[],
          ethnicity_names:[],
          bodytype_boolean: [],
          bodytype_id:[],
          bodytype_names:[],
          

         
        
      };
    


    }

    fetchCountries = async () =>{
      this.setState({loading_status:true})

               let url = urls.base_url +'api/api_country'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ countries : temp_arr});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });



    }
    

    getStates(countryId){
      this.setState({loading_status:true,state_id:'key0',city_id:'key0',cities:[],states:[]})

                       let url = urls.base_url +'api/state?country_id='+countryId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].country_id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], country_id: country_id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({states : temp_arr});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });



    }


    getCity(stateId){
      this.setState({loading_status:true,cities:[],city_id:'key0'})

                     let url = urls.base_url +'api/city?state_id='+stateId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({cities: temp_arr,loading_status:false});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                            this.setState({loading_status:false})
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });



    }

    

    setBodyType(val){
      var bodyTypeArr = val['body_type'].split(',');
      if(bodyTypeArr.length > 0){
             var length = bodyTypeArr.length;
      
       for(var i = 0 ; i < length ; i++){
        for(var j = 0 ; j < this.state.bodytype_id.length ; j++){
          if(this.state.bodytype_id[j] == bodyTypeArr[i]){
           
            let ids = [...this.state.bodytype_boolean];     // create the copy of state array
            ids[j] = true;                  //new value
            this.setState({ bodytype_boolean: ids }); 
   
           }
           else{
             //do nothing
           }
        }
        
       }
      }
    }

    fetchBodyType = async (val) =>{
      this.setState({loading_status:true})

           let url = urls.base_url +'api/api_body_type'

                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name 
                              let bodytype = [...this.state.bodytype_names];
                              // Add item to it
                              bodytype.push( name );


                            let bodytypes_ids = [...this.state.bodytype_id];  
                            bodytypes_ids.push(id );


                            let bodytypes_bool = [...this.state.bodytype_boolean];  
                            bodytypes_bool.push( false );

                            // Set state
                            this.setState({ bodytype_id:bodytypes_ids,bodytype_names:bodytype,bodytype_boolean:bodytypes_bool });
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                             // this.setState({ body : temp_arr});

                             if(val != null){
                               this.setBodyType(val)
                             }

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }

    setEthnicity(val){
      var ethnicityArr = val['ethnicity'].split(',');
      if(ethnicityArr.length > 0){
             var length = ethnicityArr.length;
      
       for(var i = 0 ; i < length ; i++){
        for(var j = 0 ; j < this.state.ethnicity_id.length ; j++){
          if(this.state.ethnicity_id[j] == ethnicityArr[i]){
           
            let ids = [...this.state.ethnicity_boolean];     // create the copy of state array
            ids[j] = true;                  //new value
            this.setState({ethnicity_boolean: ids }); 
   
           }
           else{
             //do nothing
           }
        }
        
       }
      }
    }


    fetchEthnicity = async (val) =>{
      this.setState({loading_status:true})

               let url = urls.base_url +'api/api_ethnicity'
      
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name 

                                  
                              // const array = [...temp_arr];
                              // array[i] = { ...array[i], id: id };
                              // array[i] = { ...array[i], name: name };

                              // temp_arr = array

                                     let ethnicity = [...this.state.ethnicity_names];
                                      // Add item to it
                                      ethnicity.push( name );


                                    let ethnicitys_ids = [...this.state.ethnicity_id];  
                                    ethnicitys_ids.push(id );


                                    let ethnicitys_bool = [...this.state.ethnicity_boolean];  
                                    ethnicitys_bool.push( false );

                                    // Set state
                                    this.setState({ ethnicity_id:ethnicitys_ids,ethnicity_names:ethnicity,ethnicity_boolean:ethnicitys_bool });
                                 
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                            //  this.setState({ ethnicity : temp_arr});

                              if(val != null){

                                  console.log("EEEEE",JSON.stringify(this.state.ethnicity_boolean))
                                  this.setEthnicity(val)

       
                              }

                            }


                          else{
                           // Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                           // ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    

    onSelectedItemsChange = selectedItems => {
     // ToastAndroid.show("SDfsdfsd"+JSON.stringify(selectedItems),ToastAndroid.LONG)
      this.setState({ selectedItems });
    };


   async componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')

     let val = this.props.navigation.getParam('filters_data')


     console.log('filters_data',JSON.stringify(val))


  this.fetchCountries()
   await this.fetchBodyType(val)
  await this.fetchEthnicity(val)
    


     //var result  = this.props.navigation.getParam('result')
   
     if(val == null ){
        //means filter wal called for first time 
        //so no need to set the values 
     }
     else{

   
   

      this.setState({
        weight_values : val['weight'],
        height_values : val['height'],
        stars : val['stars'],
        male_checked : val['male'],
        female_checked : val['female'],
        transgender_check : val['transgender'],
        age_values: val['age'],

      })


     
          
     }
      
    }
    multiSliderValuesChange = ( weight_values) => {
      this.setState({
          weight_values,
      });
  }
  heightmultiSliderValuesChange = (height_values) => {
    this.setState({
        height_values,
    });
 
}

agemultiSliderValuesChange = (age_values) => {
  this.setState({
      age_values,
  });

}

submit(){
  const { navigation } = this.props;
              navigation.goBack();

                    //making paramters for the ethicyt tag
            var length = this.state.ethnicity_boolean.length;
            var final= "";
            for(var i = 0 ; i < length ; i++){
              if(this.state.ethnicity_boolean[i] === false){
               //do nothing
      
              }
              else{
                let temp = this.state.ethnicity_id[i].toString()
                final = final + temp
                final = final + ","
              }
            }


                    //making paramters for the body type tag
                    var length = this.state.bodytype_boolean.length;
                    var final_body= "";
                    for(var i = 0 ; i < length ; i++){
                      if(this.state.bodytype_boolean[i] === false){
                       //do nothing
              
                      }
                      else{
                        let temp = this.state.bodytype_id[i].toString()
                        final_body = final_body + temp
                        final_body = final_body + ","
                      }
                    }




                    navigation.state.params.onSelect({ weight: this.state.weight_values ,
                      height:this.state.height_values,
                      cities:this.state.selectedItems,
                      stars:this.state.stars,
                    male:this.state.male_checked,
                        female:this.state.female_checked,
                      transgender:this.state.transgender_check,
                      ethnicity:final.length > 0 ?final.substring(0, final.length-1) : '',
                      body_type:final_body.length > 0 ? final_body.substring(0, final_body.length-1) : '',
                    age:this.state.age_values})
}






componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });




}

    render() {

      let makecountries =this.state.countries.map((country) => {
        return (
          <Item label={country.name} value={country.id} key={country.id}/>
        )
    })

        let makestates =this.state.states.map((state) => {
          return (
            <Item label={state.name} value={state.id} key={state.id}/>
          )
      })



      let locations =this.state.selectedItems.map((location) => {
        return (
         <Text>{location}</Text>
        )
    })

      let chips = this.state.ethnicity_names.map((r, i) => {

        return (
              <Chip 
              style={{width:null,margin:4}}
              mode={'outlined'}
              selected={this.state.ethnicity_boolean[i]}
            
              selectedColor={'black'}
              onPress={() => {
            
                      let ids = [...this.state.ethnicity_boolean];    
                      ids[i] =!ids[i];                  
                      this.setState({ethnicity_boolean: ids });         
              }}>
              {r}
              </Chip>
        );
})



let body_chips = this.state.bodytype_names.map((r, i) => {

  return (
        <Chip 
        style={{width:null,margin:4}}
        mode={'outlined'}
        selected={this.state.bodytype_boolean[i]}
        selectedColor={'black'}
        onPress={() => {
      
                let ids = [...this.state.bodytype_boolean];    
                ids[i] =!ids[i];                  
                this.setState({bodytype_boolean: ids });         
        }}>
        {r}
        </Chip>
  );
})

      const { selectedItems } = this.state;

      if (this.state.location_status) {
        return (
          <ScrollView>
         <View style={{flex:1,padding:20}}>


         <View style={{marginBottom:20,width:'auto'}}>
         <TouchableOpacity  onPress={()=> this.setState({location_status:false})}>

         <Image style={{width: 25, height: 25,marginTop:40,marginBottom:30,marginLeft:20}}  source={require('../assets/error.png')} />



         </TouchableOpacity>
         </View>


                     


          <Text style={{alignSelf:'center',color:'black',fontSize:16,marginBottom:25, textAlign: 'center'}}>{I18n.t('choose_desired_location_for_models')}</Text>

         <Text style={styles.input_text}>{I18n.t('country')}</Text>
         <View style={{width:'100%',borderRadius:10,borderWidth:0,borderColor:'black',marginBottom:10}}>
         <Picker

               mode="dropdown"
               selectedValue={this.state.country_id}
               onValueChange={(itemValue, itemIndex) =>
                 {
                   this.setState({country_id: itemValue})
                   if(itemValue > 0){
                    
                     this.getStates(itemValue)
                   }  
                 }}>
               <Item label="Select Country" value="key0" />
                  {makecountries}
       </Picker>
 </View>

 <Text style={styles.input_text}>{I18n.t('state')}</Text>
 <View style={{width:'100%',borderRadius:10,borderWidth:0,borderColor:'black',marginBottom:10}}>
             <Picker
   
                   mode="dropdown"
                   selectedValue={this.state.state_id}
                   onValueChange={(itemValue, itemIndex) =>
                     {
                       this.setState({state_id: itemValue,city_id:'key0'})
                       if(itemValue > 0){
                        
                         this.getCity(itemValue)
                       }  
                     }}>
                   <Item label="Select State" value="key0" />
                     {makestates}
           </Picker>
     </View>

     <Text style={styles.input_text}>{I18n.t('cities')}</Text>
     <View style={{width:'100%',borderRadius:10,borderWidth:0,borderColor:'black',margin:10}}>
         <MultiSelect
         hideTags
         items={this.state.cities}
         uniqueKey="id"
         ref={(component) => { this.multiSelect = component }}
         onSelectedItemsChange={this.onSelectedItemsChange}
         selectedItems={selectedItems}
         selectText="Choose Cities"
         searchInputPlaceholderText="Search Cities..."
         onChangeInput={ (text)=> console.log(text)}
        
         tagRemoveIconColor="#CCC"
         tagBorderColor="#CCC"
         tagTextColor="#CCC"
         selectedItemTextColor="#CCC"
         selectedItemIconColor="#CCC"
         itemTextColor="#000"
         displayKey="name"
         searchInputStyle={{ color: '#CCC' }}
         submitButtonColor="#CCC"
         submitButtonText="Submit"
       />
       </View>

       <TouchableOpacity
       onPress={()=> {
        this.setState({location_status:false}) 
       }}
      
       style={styles.loginButton}
     
       underlayColor='#fff'>
       <Text style={styles.loginText}>{I18n.t('submit').toUpperCase()}</Text>
</TouchableOpacity>
      
       <View>
        
       </View>
         
         </View>
         </ScrollView>
        );
      }



        return (
      
         <View 
         style={styles.container}>


         {/*for header*/}
         <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                  

                  
          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
            centerComponent={{ text: I18n.t('filters'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}

            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
         

       {/*for main content*/}


           <View style={styles.body}>
           <ScrollView style={{margin:10,padding:0}} showsVerticalScrollIndicator={false}>

           <Text style={{fontSize:16,marginBottom:7,fontWeight:'bold'}}>{I18n.t('interested_gender').toUpperCase()}</Text>

           <View style={{marginBottom:10}}>


             <View style={{ flexDirection: 'row' ,justifyContent:'space-between',alignItems:'center',height:Platform.OS === 'android' ? 35 : null}}>
    
                <Text style={{marginLeft:1,color:'black',fontWeight:'bold'}}>{I18n.t('female')}</Text>
                   <CheckBox
                     
                      checkedColor={colors.color_primary}
                       onPress={() => this.setState({ female_checked: !this.state.female_checked })}
                       checked={this.state.female_checked}
                     />
                   
               </View>


           <View style={{ flexDirection: 'row' ,justifyContent:'space-between',alignItems:'center',height:Platform.OS === 'android' ? 35 : null}}>
    
           <Text style={{marginLeft:1,color:'black',fontWeight:'bold'}}>{I18n.t('male')}</Text>
           <CheckBox
              
              checkedColor={colors.color_primary}
               onPress={() => this.setState({ male_checked: !this.state.male_checked })}
               checked={this.state.male_checked}
             />
           
       </View>


               <View style={{ flexDirection: 'row' ,justifyContent:'space-between',alignItems:'center',height:Platform.OS === 'android' ? 35 : null}}>
    
               <Text style={{marginLeft:1,color:'black',fontWeight:'bold'}}>{I18n.t('transgender')}</Text>
                     <CheckBox
                        
                        checkedColor={colors.color_primary}
                         onPress={() => this.setState({ transgender_checked: !this.state.transgender_checked })}
                         checked={this.state.transgender_checked}
                       />
                     
                 </View>
 
                 </View>
          

               
            <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10}}>
                <Text style={{fontSize:16,fontWeight:'bold'}}>{I18n.t('weight_kgs').toUpperCase()}</Text>
                <Text style={{color:'black',fontSize:16,fontWeight:'bold'}}>{this.state.weight_values[0]} - {this.state.weight_values[1]}</Text>
              
            </View>



           
           <View style={{width:'97%',marginLeft:Platform.OS === 'android' ? 6 : 15}}>


          

                <MultiSlider
                    values={[this.state.weight_values[0], this.state.weight_values[1]]}
                    sliderLength={Platform.OS === 'android' ? Dimensions.get('window').width *0.9 : Dimensions.get('window').width *0.85 }
                    style={{alignSelf:'center'}}
                   
                    selectedStyle={{backgroundColor: colors.color_primary}}
                    markerStyle={{backgroundColor: colors.color_primary}}
                    onValuesChange={this.multiSliderValuesChange}
                    min={40}
                    max={150}
                    step={1}
                />
               
              
            </View> 



            <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10}}>
                <Text style={{fontSize:16,fontWeight:'bold'}}>{I18n.t('height_cms').toUpperCase()}</Text>
                <Text style={{color:'black',fontSize:16,fontWeight:'bold'}}>{this.state.height_values[0]} - {this.state.height_values[1]}</Text>
              
            </View>



           
           <View style={{width:'100%',marginLeft:Platform.OS === 'android' ? 6 : 15}}>


          

                <MultiSlider
                    values={[this.state.height_values[0], this.state.height_values[1]]}
                    sliderLength={Platform.OS === 'android' ? Dimensions.get('window').width *0.9 : Dimensions.get('window').width *0.85 }
                    style={{alignSelf:'center'}}
                    selectedStyle={{backgroundColor: colors.color_primary}}
                    markerStyle={{backgroundColor: colors.color_primary}}
                    onValuesChange={this.heightmultiSliderValuesChange}
                    min={100}
                    max={220}
                    step={2}
                />
               
              
            </View> 



            <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10}}>
            <Text style={{fontSize:16,fontWeight:'bold'}}>{I18n.t('age').toUpperCase()}</Text>
            <Text style={{color:'black',fontSize:16,fontWeight:'bold'}}>{this.state.age_values[0]} - {this.state.age_values[1]}</Text>
          
        </View>


              
           <View style={{width:'100%',marginLeft:Platform.OS === 'android' ? 6 : 15}}>


          

           <MultiSlider
               values={[this.state.age_values[0], this.state.age_values[1]]}
               sliderLength={Platform.OS === 'android' ? Dimensions.get('window').width *0.9 : Dimensions.get('window').width *0.85 }
               style={{alignSelf:'center'}}
               selectedStyle={{backgroundColor: colors.color_primary}}
               markerStyle={{backgroundColor: colors.color_primary}}
               onValuesChange={this.agemultiSliderValuesChange}
               min={18}
               max={90}
               step={1}
           />
          
         
       </View> 



      






            <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,alignItems:'center'}}>
           
                <Text style={{fontSize:16,fontWeight:'bold'}}>{I18n.t('location').toUpperCase()}</Text>
                <TouchableWithoutFeedback onPress={()=> this.setState({location_status:true})}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                   <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/location.png')} />
                   <Text style={{fontSize:16,fontWeight:'bold',color:'black'}}>{I18n.t('change').toUpperCase()}</Text>
                   
                </View>
                </TouchableWithoutFeedback>
                
              
            </View>

            {/* 

              
            {locations}

            */}




{/*
        <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,alignItems:'center'}}>
        <View style={{flex:1.1}}>


                <View style={{flexDirection:'row',alignItems:'center'}}>
                <Text style={{fontSize:16,fontWeight:'bold'}}>{I18n.t('stars').toUpperCase()}</Text>
                <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/star.png')} />
            </View>
        </View>
        


        <View style={{flex:0.9,marginLeft:10}}>
                <View style={{width:'100%',borderRadius:10,borderWidth:0,borderColor:'black',marginBottom:10}}>
                <Picker

                      mode="dropdown"
                      selectedValue={this.state.stars}
                      onValueChange={(itemValue, itemIndex) =>
                        {
                          this.setState({stars: itemValue})
                          if(itemIndex > 0){
                          
                           
                          }  
                        }}>
                      <Item label="Select Stars" value="key0" />
                      <Item label="1" value="1" />
                      <Item label="2" value="2" />
                      <Item label="3" value="3" />
                      <Item label="4" value="4" />
                      <Item label="5" value="5" />
                       
              </Picker>
             </View>
        </View>
        </View>

         */}


        <Text style={{fontWeight:'bold',marginBottom:10,fontSize:16}}>{I18n.t('ethnicity').toUpperCase()}</Text>
        <View style={{flexDirection:'row',flexWrap: 'wrap'}}>
             {chips}
        </View>


        <Text style={{fontWeight:'bold',marginBottom:10,fontSize:16,marginTop:10}}>{I18n.t('body_type').toUpperCase()}</Text>
        <View style={{flexDirection:'row',flexWrap: 'wrap'}}>
             {body_chips}
        </View>
       
            <TouchableOpacity
            onPress={()=> {
              this.submit()
              
              
            }}
           
            style={styles.loginButton}
          
            underlayColor='#fff'>
            <Text style={styles.loginText}>{I18n.t('submit').toUpperCase()}</Text>
    </TouchableOpacity>

            

           

           
               


    </ScrollView>    
    {this.state.loading_status &&
      <View pointerEvents="none" style={styles.loading}>
        <ActivityIndicator size='large' />
      </View>
  }

    
    </View>

         
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%', 
    paddingTop: Platform.OS === 'android'  ? 0:10, 
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.3


  },
  input_text:{
    width:'100%',
    marginBottom:10,
    marginLeft:7,
    
    fontSize:18
  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:0,
    margin:0
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{
  
  marginTop:50,
  marginBottom:15,
  width:"100%",
  height:50,
  backgroundColor:colors.color_primary,
  borderRadius:10,
  borderWidth: 1,
  borderColor: 'black',

 
},
loginText:{
 color:'black',
 textAlign:'center',
 fontSize :20,
 paddingTop:10,
 textAlign:'center',
 color:'black'
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }
 
 
  

}
)

// var objects = [
//   {
//     "foo" : "bar",
//     "bar" : "sit"
//   },
//   {
//     "foo" : "lorem",
//     "bar" : "ipsum"
//   },
//   {
//     "foo" : "dolor",
//     "bar" : "amet"
//   }
// ];

// var results = [];

// var toSearch = "lo";

// for(var i=0; i<objects.length; i++) {
//   for(key in objects[i]) {
//     if(objects[i][key].indexOf(toSearch)!=-1) {
//       results.push(objects[i]);
//     }
//   }
// }
