import React, {Component} from 'react';
import {Text, View, Image,Modal,Dimensions,ToastAndroid,StatusBar,ScrollView,StyleSheet,FlatList,Platform,ActivityIndicator,TouchableOpacity,TextInput,TouchableWithoutFeedback,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {Card} from 'native-base';
import { isTSEnumMember } from '@babel/types';
import AsyncStorage from '@react-native-community/async-storage';
import { colors ,fonts, urls} from './Variables';
import DismissKeyboard from 'dismissKeyboard';
import RBSheet from "react-native-raw-bottom-sheet";
import ScrollPicker from 'react-native-wheel-scroll-picker';
import I18n from '../i18n';
import {Header} from 'react-native-elements';
import { StackActions, NavigationActions} from 'react-navigation';



export default class NotificationsListModel extends React.Component {
    constructor(props) {
        super(props);
        this.onFetch()
        this.state ={
          notifications:[],
          modal_visible:false,
          raise_amount:'',
          item:null,
          peoples:
        [
          {
            id: 1,
            name: 'Ronalddddoooo.',
            image:require('../assets/dummy/m1.jpg'),
            price:215,
            status:0 //0 dono button //1 accepted //2 rejected
            
          },
          {
            id: 2,
            name: 'Steve ',
            image:require('../assets/dummy/m2.jpg'),
            price:342,
            status:1
            
          },
         
        

          ]
          
      };


    }



    // static navigationOptions = ({ navigation }) => {
    //   const { params } = navigation.state;
  
    //   return {
    //     title: params ? params.screenTitle: 'Lists',
    //   }
    // };


    onAccept(member_id,bid_id){

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();


          formData.append('model_id',item);
          formData.append('member_id',member_id);
          formData.append('id',bid_id);
          

                        this.setState({loading_status:true})
                        let url = urls.base_url +'api/accept'
                        fetch(url, {
                        method: 'POST',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },
                    body: formData

                      }).then((response) => response.json())
                            .then((responseJson) => {
                             this.setState({loading_status:false})
                              //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                if(!responseJson.error){

                                  var length = this.state.notifications.length.toString();

                                 
                                  for(var i = 0 ; i < length ; i++){
                                 
                                        if(this.state.notifications[i].nid == bid_id){
                                        //  this.state.notifications[i].accept = 1
                                         
                                        //  this.setState({notifications})

                                          let notifications = [ ...this.state.notifications ];
                                          notifications[i] = {...notifications[i],accept:1};
                                            this.setState({ notifications});
                                        }
                                              
                                  }
                                        
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                              }
                              else{

                               
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})
                              
                                // Platform.OS === 'android' 
                                // ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                // : Alert.alert("Connection Error !")
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show(JSON.stringify(error), ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
        //  ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

      


    

    }


    onReject(member_id,bid_id){

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();


          formData.append('model_id',item);
          formData.append('member_id',member_id);
          formData.append('id',bid_id);
          

                        this.setState({loading_status:true})
                        let url = urls.base_url +'api/reject'
                        fetch(url, {
                        method: 'POST',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },
                    body: formData

                      }).then((response) => response.json())
                            .then((responseJson) => {
                            this.setState({loading_status:false})
                              // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                if(!responseJson.error){
                                  var length = this.state.notifications.length.toString();

                                 
                                  for(var i = 0 ; i < length ; i++){
                                 
                                        if(this.state.notifications[i].nid == bid_id){
                                        //  this.state.notifications[i].reject = 1
                                         
                                        //  this.setState({notifications})

                                          let notifications = [ ...this.state.notifications ];
                                          notifications[i] = {...notifications[i],reject:1};
                                            this.setState({ notifications});
                                        }
                                              
                                  }


                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                                    
                              }
                              else{

                              
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})
                              
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

    }

    onRaise(member_id,bid_id,raise_price){
     // ToastAndroid.show("raisding....."+member_id,ToastAndroid.LONG)

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();


          formData.append('model_id',item);
          formData.append('member_id',member_id);
          formData.append('id',bid_id);
          formData.append('raise_amount',raise_price);
          

                        this.setState({loading_status:true})
                        let url = urls.base_url +'api/raise'
                        fetch(url, {
                        method: 'POST',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },
                    body: formData

                      }).then((response) => response.json())
                            .then((responseJson) => {
                            this.setState({loading_status:false,raise_amount:''})
                             //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                if(!responseJson.error){

                                  var length = this.state.notifications.length.toString();

                                 
                                  for(var i = 0 ; i < length ; i++){
                                 
                                        if(this.state.notifications[i].nid == bid_id){
                                          let notifications = [ ...this.state.notifications ];
                                          notifications[i] = {...notifications[i],raise:1};
                                          notifications[i] = {...notifications[i],accept:0};
                                          notifications[i] = {...notifications[i],reject:0};
                                            this.setState({ notifications});


                                        //  this.state.notifications[i].raise = 1
                                        //  this.state.notifications[i].accept = 0
                                        //  this.state.notifications[i].reject = 0
                                         
                                        //  this.setState({notifications})
                                        }
                                              
                                  }


                                
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                                    
                              }
                              else{
                                ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})
                              
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

    }


    onFetch(){
     

        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
            var formData = new FormData();


            formData.append('model_id',item);
            
           // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
                          this.setState({loading_status:true})
                          let url = urls.base_url +'api/notification_list_model'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                               this.setState({loading_status:false})
                               // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                  if(!responseJson.error){
                                    var temp_arr=[]
                                    var length = responseJson.Notifications.length.toString();
                                    //ToastAndroid.show(JSON.stringify(length), ToastAndroid.SHORT);
                                    for(var i = 0 ; i < length ; i++){
                                      var notificaiton_id = responseJson.Notifications[i].id
                                      var bid_price = responseJson.Notifications[i].bid_price
                                      var accept = responseJson.Notifications[i].accept
                                      var reject = responseJson.Notifications[i].reject
                                      var raise = responseJson.Notifications[i].raise

                                      var user_id = responseJson.Notifications[i].user.id
                                     // var user_name = responseJson.Notifications[i].user.user_name
                                     var user_name = responseJson.Notifications[i].user.name
                                      var user_image = responseJson.Notifications[i].user.user_image
                                      
                                     
                                            const array = [...temp_arr];
                                            array[i] = { ...array[i], nid: notificaiton_id };
                                            array[i] = { ...array[i], price: bid_price };
                                            array[i] = { ...array[i], accept: accept };
                                            array[i] = { ...array[i], reject:reject };
                                            array[i] = { ...array[i], raise: raise };

                                            array[i] = { ...array[i], uid:user_id};
                                            array[i] = { ...array[i], user_name:user_name };
                                            array[i] = { ...array[i], user_image: user_image };
                                           
        
                                            temp_arr = array
                                            
                                            //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                      }
                                      this.setState({ notifications : temp_arr});
                                     
                                        
                                }
                                else{

                                 
                                    Platform.OS === 'android' 
                                    ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                    : Alert.alert(responseJson.message)
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                  : Alert.alert("Connection Error !")
                              });
          }
          else{
           // ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });

        


      

}


  Fetch(){
     

  AsyncStorage.getItem("user_id").then((item) => {
    if (item) {
      var formData = new FormData();


      formData.append('model_id',item);
      

                   // this.setState({loading_status:true})
                   let url = urls.base_url +'api/notification_list_model'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                         this.setState({loading_status:false})
                         // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                            if(!responseJson.error){
                              var temp_arr=[]
                              var length = responseJson.Notifications.length.toString();
                              //ToastAndroid.show(JSON.stringify(length), ToastAndroid.SHORT);
                              for(var i = 0 ; i < length ; i++){
                                var notificaiton_id = responseJson.Notifications[i].id
                                var bid_price = responseJson.Notifications[i].bid_price
                                var accept = responseJson.Notifications[i].accept
                                var reject = responseJson.Notifications[i].reject
                                var raise = responseJson.Notifications[i].raise

                                var user_id = responseJson.Notifications[i].user.id
                                var user_name = responseJson.Notifications[i].user.name
                                var user_image = responseJson.Notifications[i].user.user_image
                                
                               
                                      const array = [...temp_arr];
                                      array[i] = { ...array[i], nid: notificaiton_id };
                                      array[i] = { ...array[i], price: bid_price };
                                      array[i] = { ...array[i], accept: accept };
                                      array[i] = { ...array[i], reject:reject };
                                      array[i] = { ...array[i], raise: raise };

                                      array[i] = { ...array[i], uid:user_id};
                                      array[i] = { ...array[i], user_name:user_name };
                                      array[i] = { ...array[i], user_image: user_image };
                                     
  
                                      temp_arr = array
                                      
                                      //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                }
                                this.setState({ notifications : temp_arr});
                               
                                  
                          }
                          else{

                           
                              Platform.OS === 'android' 
                              ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})
                          
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                        });
    }
    else{
     // ToastAndroid.show("User not found !",ToastAndroid.LONG)
    }
  });

  




}

    modal(item){
      return(

          
            <Modal
                visible={this.state.modal_visible}
                animationType='slide'
                onRequestClose={() => {console.log('Modal has been closed.');}}
                transparent
                >
                      <View style={{height:'100%',width:'100%',backgroundColor:'rgba(0, 0, 0, 0.7)',alignItems:'center',justifyContent:'center'}}>
                      <View style={{height:null,width:300,backgroundColor:'white',alignItems:'center'}}>

                      <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:10,marginBottom:10}}>{I18n.t('enter_raise_amount')} </Text>

                      <Text style={styles.input_text}>{I18n.t('enter_amount_dollars')}</Text>
                      <TextInput
                        value={this.state.raise_amount}
                      
                        maxLength={20}
                        onChangeText={ (raise_amount) => {
                          var a = raise_amount.replace(/[^0-9.]/g, '')
                          this.setState({raise_amount:a})
                        }}
                      
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        keyboardType = 'numeric'
                        textContentType='telephoneNumber'
                        style={styles.input}
                        placeholderTextColor={'black'}
                      />
                        <TouchableOpacity
                        style={{backgroundColor:colors.color_primary,width:'100%'}}
                          onPress={() => {
                            ToastAndroid.show(this.state.item.uid+"...."+this.state.item.nid+"..."+this.state.raise_amount,ToastAndroid.SHORT)
                            this.onRaise(this.state.item.uid,this.state.item.nid,this.state.raise_amount)
                                this.setState({modal_visible:!this.state.modal_visible})
                              
                              }}
                          >
                          <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:14,marginBottom:14,alignSelf:'center'}}>{I18n.t('raise')}</Text>
                          </TouchableOpacity>
                      </View>
                    
                        </View>
              </Modal>
      )
    }


item(accept,reject,raise,item){
  if(accept == 0 && reject == 0 && raise == 0 ){
    return(
      <View style={{flexDirection:'row'}}>

      <TouchableWithoutFeedback onPress={()=>{
        this.setState({item:item})
        this.onAccept(item.uid,item.nid)
      }}>
      <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
     </TouchableWithoutFeedback>

     <TouchableWithoutFeedback onPress={()=>{
      this.setState({item:item})
      this.onReject(item.uid,item.nid)
    }}>
      <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />
        </TouchableWithoutFeedback>

        <TouchableOpacity onPress={()=>{
          this.setState({modal_visible:true,item:item,raise_amount:item.price})
        // ToastAndroid.show("rpice....."+item.price,ToastAndroid.LONG)
        }}>
        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/raise.png')} />
        </TouchableOpacity>


      </View>
      );
  }
  else if(accept == 0 && reject == 0 && raise == 1){
    return(
      <View style={{flexDirection:'row'}}>

      <TouchableWithoutFeedback onPress={()=>{
        this.setState({item:item})
        this.onAccept(item.uid,item.nid)
      }}>
      <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
     </TouchableWithoutFeedback>

     <TouchableWithoutFeedback onPress={()=>{
      this.setState({item:item})
      this.onReject(item.uid,item.nid)
    }}>
      <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />
        </TouchableWithoutFeedback>

        <TouchableOpacity onPress={()=>{
          this.setState({modal_visible:true,item:item,raise_amount:item.price})
        // ToastAndroid.show("rpice....."+item.price,ToastAndroid.LONG)
        }}>
        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/raise.png')} />
        </TouchableOpacity>


      </View>
      );

  }
  else if(accept == 0 && reject == 0 && raise == 2 ){
    return(
      <TouchableWithoutFeedback>
      <View style={{borderColor:'orange',borderRadius:8,borderWidth:2}}>
      
      <Text style={{color:'orange',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('raised')}</Text>
      
      </View>
      </TouchableWithoutFeedback>
      )

  }


  else if(accept == 1 ){
    //means  accepted

          
    return(
      // <TouchableWithoutFeedback onPress={()=>{
      //   this.onAccept(item.uid,item.nid)
      // }}>

      <TouchableWithoutFeedback>
      <View style={{borderColor:'green',borderRadius:8,borderWidth:2}}>
      
      <Text style={{color:'green',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('accepted')}</Text>
      
      </View>
      </TouchableWithoutFeedback>
      )

  }
  else if(reject == 1){
    return(
    // <TouchableWithoutFeedback onPress={()=>{
    //   this.onReject(item.uid,item.nid)
    // }}>
    <TouchableWithoutFeedback>
    <View style={{borderColor:'red',borderRadius:8,borderWidth:2}}>
    
    <Text style={{color:'red',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('rejected')}</Text>
    
    </View>
    </TouchableWithoutFeedback>
    )
  }

  else if(raise == 1){
    return(
      <View style={{flexDirection:'row',justifyContent:'space-around'}}>
      <TouchableOpacity>
                <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
      </TouchableOpacity>

        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />



</View>
);
  }
  else{
    return(
      <View style={{flexDirection:'row'}}>

      <TouchableWithoutFeedback onPress={()=>{
        this.setState({item:item})
        this.onAccept(item.uid,item.nid)
      }}>
      <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
     </TouchableWithoutFeedback>

     <TouchableWithoutFeedback onPress={()=>{
      this.setState({item:item})
      this.onReject(item.uid,item.nid)
    }}>
      <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />
        </TouchableWithoutFeedback>

        <TouchableOpacity onPress={()=>{
          this.setState({modal_visible:true,item:item,raise_amount:item.price})
         
        }}>
        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/raise.png')} />
        </TouchableOpacity>


      </View>
      )
  }


  
}

componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
              this.props.navigation.setParams({screenTitle: I18n.t('lists')})
          }
          else {
               I18n.locale = 'en'
            }
  });



}

componentWillUnmount(){
  clearInterval(this.timer)
}

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     this.onFetch()
     this.timer = setInterval(()=> this.Fetch(), 4000)
      
    }
   

    next(member_id){
      let obj ={
        "member_id":member_id
      }
      this.props.navigation.navigate("MemberProfile",{result:obj})
    }

    raiseCheck(){
          if(this.state.raise_amount > 0){
            this.onRaise(this.state.item.uid,this.state.item.nid,this.state.raise_amount)
            this.setState({modal_visible:!this.state.modal_visible})
            
          }
          else{
            Platform.OS == 'android'
            ?
            ToastAndroid.show("Enter Raise Amount !",ToastAndroid.LONG)
            : 
            Alert.alert("Enter Raise Amount !")
          }
    }

    render() {
        return (
      
         <View 
         style={styles.container}>


         <Modal
         visible={this.state.modal_visible}
         animationType='slide'
         onRequestClose={() => {console.log('Modal has been closed.');}}
         transparent
         >
               <View style={{height:'100%',width:'100%',backgroundColor:'rgba(0, 0, 0, 0.7)',alignItems:'center',justifyContent:'center'}}>
               <View style={{height:null,width:300,backgroundColor:'white',alignItems:'center'}}>

                      <View style={{position:'absolute',right:10,top:10}}>
                <TouchableWithoutFeedback onPress={()=> this.setState({
                  modal_visible:false
                })}>
                <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:20,width:20}} />
                </TouchableWithoutFeedback>
                </View>

               <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:20,marginBottom:10}}>{I18n.t('enter_raise_amount')} </Text>

               <Text style={styles.input_text}>{I18n.t('enter_amount_dollars')}</Text>

                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',margin:5}}>
                    <TouchableWithoutFeedback onPress={()=> this.setState({raise_amount : this.state.raise_amount > 10 ?  this.state.raise_amount - 10 : this.state.raise_amount})}>
                      <Image source={require('../assets/minus.png')} style={{alignSelf:'center',height:25,width:25,marginRight:10}} />
                      </TouchableWithoutFeedback>

                  <View style={{paddingLeft:9,paddingRight:9,paddingTop:2,paddingBottom:2,borderColor:'black',borderRadius:9,borderWidth:2}}>
                  <Text style={{fontSize:18,color:'black'}}>{this.state.raise_amount}</Text>
                  </View>

                 


                  <TouchableWithoutFeedback onPress={()=> this.setState({raise_amount : this.state.raise_amount + 10})}>
                      <Image source={require('../assets/plus.png')} style={{alignSelf:'center',height:25,width:25,marginLeft:10}} />
                      </TouchableWithoutFeedback>
               </View>


                 <TouchableOpacity
                 style={{backgroundColor:colors.color_primary,width:'100%',marginTop:7}}
                   onPress={() => {
                     this.raiseCheck()
                   // ToastAndroid.show(this.state.item.uid+"...."+this.state.item.nid+"..."+this.state.raise_amount,ToastAndroid.SHORT)
                  }}
                   >
                   <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:14,marginBottom:14,alignSelf:'center'}}>{I18n.t('raise')}</Text>
                   </TouchableOpacity>
               </View>
              
                 </View>
       </Modal>


            {/*for header*/}
            <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
            this.props.navigation.navigate("HomeScreen")
            const resetAction = StackActions.reset({
                index: 0,
                key: 'HomeScreen',
                actions: [NavigationActions.navigate({ routeName: 'HomePage' })],
              });
              this.props.navigation.dispatch(resetAction);
          }}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                
          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
           
        centerComponent={{ text: I18n.t('bids'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />
        
       {/*for main content*/}


           <View style={styles.body}>
           <ScrollView style={{width:'100%',flex:1}} showsVerticalScrollIndicator={false}>

              
           <FlatList
							  style={{marginBottom:10}}
                data={this.state.notifications}
							  showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                inverted
							  renderItem={({item}) =>

							  
                 <View style={{width:'98%',margin:3}}>
                 
									  <Card style={{width:'100%',padding:10,borderRadius:10}}>
                    <TouchableWithoutFeedback onPress={() =>this.next(item.uid)}>


                    
                   
                      <View style={{width:'100%',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Image style={{width: 50, height: 50,borderRadius:25,overflow:'hidden',marginRight:10}}  source={{uri:urls.base_url+item.user_image}} />

                            <View>
                            <Text style={{fontSize:17,color:'black',marginRight:4}}>{item.user_name}</Text>
                            <Text style={{fontSize:14,color:'grey',marginRight:4}}>{I18n.t('bid_price')} : ${item.price}</Text>
                            
                            </View>
                            </View>

                                  <View style={{marginTop:0}}>

                                  {
                                    this.item(item.accept,item.reject,item.raise,item)

                                  }
                                  </View>
                       

                      </View>
                     

                        </TouchableWithoutFeedback> 
                    </Card>
                    
                    </View>
							 
							  }
							  keyExtractor={item => item.id}
              />
              </ScrollView>

              <RBSheet
              ref={ref => {
                  this.RBSheet = ref;
              }}
              height={400}
              duration={250}
              customStyles={{
                  container: {
                  justifyContent: "center",
                  alignItems: "center"
                  }
              }}
              >
     
              <View style={{height:'100%',justifyContent:'center',alignItems:'center',width:'100%'}}>
              <View style={{height:'75%'}}>
     
              <Text style={{fontSize:18,margin:10,color:'black',marginTop:-5,marginBottom:20}}>{I18n.t('choose_bid_amount_dollars')}</Text>
              <ScrollPicker
                   dataSource={[
                    '10',
                    '20',
                    '30',
                    '40',
                    '50',
                    '60',
                    '70',
                    '80',
                    '90',
                    '100',
                    '110',
                    '120',
                    '130',
                    '140',
                    '150',
                    '160',
                    '170',
                    '180',
                    '190',
                    '200',
                    '220',
                    '240',
                    '260',
                    '280',
                    '300',
                    '320',
                    '340',
                    '360',
                    '380',
                    '400',
                    '420',
                    '440',
                    '460',
                    '480',
                    '500',
                    '520',
                    '540',
                    '560',
                    '580',
                    '600',
                    '620',
                    '640',
                    '660',
                    '680',
                    '700',
                    '720',
                    '740',
                    '760',
                    '760',
                    '780',
                    '800',
                    '820',
                    '840',
                    '860',
                    '880',
                    '900',
                    '920',
                    '940',
                    '960',
                    '980',
                    '1000',
                    '1020',
                    '1040',
                    '1060',
                    '1080',
                    '1100',
                    '1120',
                    '1140',
                    '1160',
                    '1180',
                    '1200',
                    '1220',
                    '1240',
                    '1260',
                    '1280',
                    '1300',
                    '1320',
                    '1340',
                    '1360',
                    '1380',
                    '1400',
                    '1420',
                    '1440',
                    '1460',
                    '1480',
                    '1500',
                    '1520',
                    '1540',
                    '1560',
                    '1580',
                    '1600',
                    '1620',
                    '1640',
                    '1660',
                    '1680',
                    '1700',
                    '1720',
                    '1740',
                    '1760',
                    '1760',
                    '1780',
                    '1800',
                    '1820',
                    '1840',
                    '1860',
                    '1880',
                    '1900',
                    '1920',
                    '1940',
                    '1960',
                    '1980',
                    '2000'
     
                   ]}
                   selectedIndex={10}
                   renderItem={(data, index, isSelected) => {
                     
                       //
                   }}
                   onValueChange={(data, selectedIndex) => {
                       //
                       this.setState({
                           index:selectedIndex,
                           raise_amount:data
                       })
                       //ToastAndroid.show(data,ToastAndroid.SHORT)
                      
                   }}
                   wrapperHeight={70}
                   wrapperWidth={150}
                   wrapperBackground={'#FFFFFF'}
                   itemHeight={40}
                   highlightColor={'#d8d8d8'}
                   highlightBorderWidth={4}
                   activeItemColor={'#222121'}
                   itemColor={'#B4B4B4'}
                 />
                 </View>
     
     
               
                 <TouchableOpacity
                 style={styles.submitButton}
                 onPress={()=>{
                   this.RBSheet.close()
                 }}
     
     
                 >
                 <Text style={styles.submitText}>{I18n.t('select').toUpperCase()}</Text>
                 </TouchableOpacity>
              
                
     
                                   
                  </View>
              </RBSheet>

                
    
           </View>

           {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }

         
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%', 
    paddingTop: Platform.OS === 'android'  ? 0:10, 
    backgroundColor: colors.color_primary

  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    backgroundColor:'#F5F0F0',
    padding:5,
    margin:0
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  
 input_text:{
  width:'100%',
  margin:7,
  fontSize:fonts.font_size,
  textAlign:'center'
  

 },
 input: {
  width: "80%",
  height: 50,
  textAlign: 'center',
  borderRadius: 10,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
  marginLeft:20,
  marginRight:20,
  
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},

submitButton:{
    
  marginBottom:5,
  marginTop:10,
  width:"90%",
  height:50,
  backgroundColor:colors.color_primary,
  borderRadius:10,
  borderWidth: 1,
  borderColor: 'black',

 
},
submitText:{
 color:'black',
 textAlign:'center',
 fontSize :18,
 padding:10
 
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}
 
 
  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}